using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReceivedInput
{
   public SNibble input;

    public void Receive(SNibble nib){
        input = nib;
    }
}
