using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;


public class TransmittersTest
{
    private Transmitters _transmitters;
    private GameObject _testObject;
    private ReceivedInput _receivedInput;

    [SetUp]
    public void SetUp() {
        GameObject transmitterObject = GameObject.Instantiate(new GameObject());
        Transmitter transmitter = transmitterObject.AddComponent<Transmitter>();
        _testObject = GameObject.Instantiate(new GameObject());
        _transmitters = _testObject.AddComponent<Transmitters>();
        _transmitters.connectorPrefab = transmitterObject;
        _transmitters.numberOfConnectors = 1;
        _receivedInput = new ReceivedInput();
    }

    [UnityTest]
    public IEnumerator TestSendBitValueZero()
    {
        Transmitter transmitter = _testObject.GetComponentInChildren<Transmitter>();
        transmitter.Subscribe(_receivedInput.Receive);

        SNibble nib = new SNibble();
        nib.RawValue = 0b_0;
        _transmitters.processWholeNibble = false;
        _transmitters.SendSignal(nib);

        yield return new WaitForSeconds(0.01f);

        Assert.AreEqual(SNibble.ZeroValue, _receivedInput.input);
    }

    [UnityTest]
    public IEnumerator TestSendBitZeroValueAtReceiverPosition()
    {
        Transmitter transmitter = _testObject.GetComponentInChildren<Transmitter>();
        transmitter.Subscribe(_receivedInput.Receive);

        SNibble nib = new SNibble();
        nib.RawValue = 0b_10;
        _transmitters.processWholeNibble = false;
        _transmitters.SendSignal(nib);

        yield return new WaitForSeconds(0.01f);

        Assert.AreEqual(SNibble.ZeroValue, _receivedInput.input);
    }

    [UnityTest]
    public IEnumerator TestSendBitOneValueAtReceiverPosition()
    {
        Transmitter transmitter = _testObject.GetComponentInChildren<Transmitter>();
        transmitter.Subscribe(_receivedInput.Receive);

        SNibble nib = new SNibble();
        nib.RawValue = 0b_11;
        _transmitters.processWholeNibble = false;
        _transmitters.SendSignal(nib);

        yield return new WaitForSeconds(0.01f);

        Assert.AreEqual(SNibble.MaxValue, _receivedInput.input);
    }

    [UnityTest]
    public IEnumerator TestProcessWholeNibbleZeroValue()
    {
        Transmitter transmitter = _testObject.GetComponentInChildren<Transmitter>();
        transmitter.Subscribe(_receivedInput.Receive);

        SNibble nib = new SNibble();
        nib.RawValue = 0b_0;
        _transmitters.processWholeNibble = true;
        _transmitters.SendSignal(nib);

        yield return new WaitForSeconds(0.01f);

        Assert.AreEqual(nib, _receivedInput.input);
    }

    [UnityTest]
    public IEnumerator TestProcessWholeNibbleMaxValue()
    {
        Transmitter transmitter = _testObject.GetComponentInChildren<Transmitter>();
        transmitter.Subscribe(_receivedInput.Receive);

        SNibble nib = new SNibble();
        nib.RawValue = 0b_111;
        _transmitters.processWholeNibble = true;
        _transmitters.SendSignal(nib);

        yield return new WaitForSeconds(0.01f);

        Assert.AreEqual(nib.RawValue, _receivedInput.input.RawValue);
    }

    [UnityTest]
    public IEnumerator TestProcessWholeNibbleNegativeValue()
    {
        Transmitter transmitter = _testObject.GetComponentInChildren<Transmitter>();
        transmitter.Subscribe(_receivedInput.Receive);

        SNibble nib = new SNibble();
        nib.RawValue = 0b_1011;
        _transmitters.processWholeNibble = true;
        _transmitters.SendSignal(nib);

        yield return new WaitForSeconds(0.01f);

        Assert.AreEqual(nib, _receivedInput.input);
    }
}
