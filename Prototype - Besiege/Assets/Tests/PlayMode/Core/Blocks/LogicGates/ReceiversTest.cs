using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

public class ReceiversTest
{

    private GameObject _testObject;
    private GameObject _receiverObject;
    private Receivers _receivers;
    private Receiver _receiver;

    private ReceivedInput _receivedInput;

    [SetUp]
    public void SetUp() {
        _receiverObject = new GameObject();
        _receiver = _receiverObject.AddComponent<Receiver>();

        _testObject = GameObject.Instantiate(new GameObject());

        _receivers = _testObject.AddComponent<Receivers>();
        _receivers.connectorPrefab = _receiverObject;
        _receivers.numberOfConnectors = 1;
        _receivedInput = new ReceivedInput();
    }



    [UnityTest]
    public IEnumerator TestReceiveBitValueZero()
    {
        GameObject transmitterObject = new GameObject();
        Transmitter transmitter = transmitterObject.AddComponent<Transmitter>();

        _receiver = _testObject.GetComponentInChildren<Receiver>();
        _receiver.SubscribeToTransmitter(transmitter);
        _receivers.Subscribe(_receivedInput.Receive);
        _receivers.processWholeNibble = false;

        SNibble nib = new SNibble();
        nib.RawValue = 0b_0;

        transmitter.SendSignal(nib);

        yield return new WaitForSeconds(0.01f);

        Assert.AreEqual(SNibble.ZeroValue, _receivedInput.input, "Received value: " + _receivedInput.input.RawValue);
    }

    [UnityTest]
    public IEnumerator TestReceiveBitValueNegativeZero()
    {
        GameObject transmitterObject = new GameObject();
        Transmitter transmitter = transmitterObject.AddComponent<Transmitter>();

        _receiver = _testObject.GetComponentInChildren<Receiver>();
        _receiver.SubscribeToTransmitter(transmitter);
        _receivers.Subscribe(_receivedInput.Receive);
        _receivers.processWholeNibble = false;

        SNibble nib = new SNibble();
        nib.RawValue = 0b_1000;

        transmitter.SendSignal(nib);

        yield return new WaitForSeconds(0.01f);


        Assert.AreEqual(SNibble.ZeroValue, _receivedInput.input, "Received value: " + _receivedInput.input.RawValue);
    }

    [UnityTest]
    public IEnumerator TestReceiveBitPositiveValue()
    {
        GameObject transmitterObject = new GameObject();
        Transmitter transmitter = transmitterObject.AddComponent<Transmitter>();

        _receiver = _testObject.GetComponentInChildren<Receiver>();
        _receiver.SubscribeToTransmitter(transmitter);
        _receivers.Subscribe(_receivedInput.Receive);
        _receivers.processWholeNibble = false;

        SNibble nib = new SNibble();
        nib.RawValue = 0b_11;

        transmitter.SendSignal(nib);

        yield return new WaitForSeconds(0.01f);

        nib.RawValue = 0b_1;

        Assert.AreEqual(nib, _receivedInput.input, "Received value: " + _receivedInput.input.RawValue);
    }

    [UnityTest]
    public IEnumerator TestReceiveBitNegativeValue()
    {
        GameObject transmitterObject = new GameObject();
        Transmitter transmitter = transmitterObject.AddComponent<Transmitter>();

        _receiver = _testObject.GetComponentInChildren<Receiver>();
        _receiver.SubscribeToTransmitter(transmitter);
        _receivers.Subscribe(_receivedInput.Receive);
        _receivers.processWholeNibble = false;

        SNibble nib = new SNibble();
        nib.RawValue = 0b_1001;

        transmitter.SendSignal(nib);

        yield return new WaitForSeconds(0.01f);

        nib.RawValue = 0b_1;

        Assert.AreEqual(nib, _receivedInput.input, "Received value: " + _receivedInput.input.RawValue);
    }

    [UnityTest]
    public IEnumerator TestReceiveBitMultipleTransmittersWaitsForFullInput()
    {
        GameObject transmitterObject = new GameObject();
        Transmitter transmitter1 = transmitterObject.AddComponent<Transmitter>();
        Transmitter transmitter2 = transmitterObject.AddComponent<Transmitter>();
        Transmitter transmitter3 = transmitterObject.AddComponent<Transmitter>();

        _receiver = _testObject.GetComponentInChildren<Receiver>();
        _receiver.SubscribeToTransmitter(transmitter1);
        _receiver.SubscribeToTransmitter(transmitter2);
        _receiver.SubscribeToTransmitter(transmitter3);
        _receivers.Subscribe(_receivedInput.Receive);
        _receivers.processWholeNibble = true;
        _receivedInput.input = SNibble.ZeroValue;

        SNibble nib = new SNibble();
        nib.RawValue = 0b_1;

        transmitter1.SendSignal(nib);
        transmitter2.SendSignal(nib);

        yield return new WaitForSeconds(0.01f);

        Assert.AreEqual(SNibble.ZeroValue, _receivedInput.input,
                        "Received value should be 0. Receiver should wait for receiving all the signals from transmitters" + _receivedInput.input.RawValue);

        transmitter3.SendSignal(SNibble.ZeroValue);

        yield return new WaitForSeconds(0.01f);

        Assert.AreEqual(nib, _receivedInput.input, "Should receive value.");
    }

    [UnityTest]
    public IEnumerator TestReceiveBitMultipleTransmittersZeroValue()
    {
        GameObject transmitterObject = new GameObject();
        Transmitter transmitter1 = transmitterObject.AddComponent<Transmitter>();
        Transmitter transmitter2 = transmitterObject.AddComponent<Transmitter>();
        Transmitter transmitter3 = transmitterObject.AddComponent<Transmitter>();

        _receiver = _testObject.GetComponentInChildren<Receiver>();
        _receiver.SubscribeToTransmitter(transmitter1);
        _receiver.SubscribeToTransmitter(transmitter2);
        _receivers.Subscribe(_receivedInput.Receive);
        _receivers.processWholeNibble = false;
        _receivedInput.input = SNibble.ZeroValue;

        SNibble nib = new SNibble();
        nib.RawValue = 0b_1000;

        transmitter1.SendSignal(SNibble.ZeroValue);
        transmitter2.SendSignal(nib);

        yield return new WaitForSeconds(0.01f);

        Assert.AreEqual(SNibble.ZeroValue, _receivedInput.input,
                        "Received value should be 0" + _receivedInput.input.RawValue);
    }

    [UnityTest]
    public IEnumerator TestReceiveBitMultipleTransmittersNonZeroValue()
    {
        GameObject transmitterObject = new GameObject();
        Transmitter transmitter1 = transmitterObject.AddComponent<Transmitter>();
        Transmitter transmitter2 = transmitterObject.AddComponent<Transmitter>();
        Transmitter transmitter3 = transmitterObject.AddComponent<Transmitter>();

        _receiver = _testObject.GetComponentInChildren<Receiver>();
        _receiver.SubscribeToTransmitter(transmitter1);
        _receiver.SubscribeToTransmitter(transmitter2);
        _receivers.Subscribe(_receivedInput.Receive);
        _receivers.processWholeNibble = false;
        _receivedInput.input = SNibble.ZeroValue;


        SNibble nibPositive = new SNibble();
        SNibble nibNegative = new SNibble();
        nibPositive.RawValue = 0b_0001;
        nibNegative.RawValue = 0b_1111;

        transmitter1.SendSignal(nibPositive);
        transmitter2.SendSignal(nibNegative);

        yield return new WaitForSeconds(0.01f);

        Assert.AreEqual(nibPositive, _receivedInput.input,
                        "Received value should be 0" + _receivedInput.input.RawValue);
    }

    [UnityTest]
    public IEnumerator TestReceiveWholeNibbleZero()
    {
        GameObject transmitterObject = new GameObject();
        Transmitter transmitter = transmitterObject.AddComponent<Transmitter>();

        _receiver = _testObject.GetComponentInChildren<Receiver>();
        _receiver.SubscribeToTransmitter(transmitter);
        _receivers.Subscribe(_receivedInput.Receive);
        _receivers.processWholeNibble = true;

        SNibble nib = new SNibble();
        nib.RawValue = 0b_0;

        transmitter.SendSignal(nib);

        yield return new WaitForSeconds(0.01f);

        Assert.AreEqual(SNibble.ZeroValue, _receivedInput.input, "Received value: " + _receivedInput.input.RawValue);
    }

    [UnityTest]
    public IEnumerator TestReceiveWholeNibbleNegativeZero()
    {
        GameObject transmitterObject = new GameObject();
        Transmitter transmitter = transmitterObject.AddComponent<Transmitter>();

        _receiver = _testObject.GetComponentInChildren<Receiver>();
        _receiver.SubscribeToTransmitter(transmitter);
        _receivers.Subscribe(_receivedInput.Receive);
        _receivers.processWholeNibble = true;

        SNibble nib = new SNibble();
        nib.RawValue = 0b_1000;

        transmitter.SendSignal(nib);

        yield return new WaitForSeconds(0.01f);

        Assert.AreEqual(SNibble.ZeroValue, _receivedInput.input, "Received value: " + _receivedInput.input.RawValue);
    }

    [UnityTest]
    public IEnumerator TestReceiveWholeNibblePositiveValue()
    {
        GameObject transmitterObject = new GameObject();
        Transmitter transmitter = transmitterObject.AddComponent<Transmitter>();

        _receiver = _testObject.GetComponentInChildren<Receiver>();
        _receiver.SubscribeToTransmitter(transmitter);
        _receivers.Subscribe(_receivedInput.Receive);
        _receivers.processWholeNibble = true;

        SNibble nib = new SNibble();
        nib.RawValue = 0b_0101;

        transmitter.SendSignal(nib);

        yield return new WaitForSeconds(0.01f);

        Assert.AreEqual(nib, _receivedInput.input, "Received value: " + _receivedInput.input.RawValue);
    }

    [UnityTest]
    public IEnumerator TestReceiveWholeNibbleNegativeValue()
    {
        GameObject transmitterObject = new GameObject();
        Transmitter transmitter = transmitterObject.AddComponent<Transmitter>();

        _receiver = _testObject.GetComponentInChildren<Receiver>();
        _receiver.SubscribeToTransmitter(transmitter);
        _receivers.Subscribe(_receivedInput.Receive);
        _receivers.processWholeNibble = true;

        SNibble nib = new SNibble();
        nib.RawValue = 0b_1001;

        transmitter.SendSignal(nib);

        yield return new WaitForSeconds(0.01f);

        Assert.AreEqual(nib, _receivedInput.input, "Received value: " + _receivedInput.input.RawValue);
    }

    [UnityTest]
    public IEnumerator TestReceiveWholeNibbleMultipleTransmittersWaitsForFullInput()
    {
        GameObject transmitterObject = new GameObject();
        Transmitter transmitter1 = transmitterObject.AddComponent<Transmitter>();
        Transmitter transmitter2 = transmitterObject.AddComponent<Transmitter>();
        Transmitter transmitter3 = transmitterObject.AddComponent<Transmitter>();

        _receiver = _testObject.GetComponentInChildren<Receiver>();
        _receiver.SubscribeToTransmitter(transmitter1);
        _receiver.SubscribeToTransmitter(transmitter2);
        _receiver.SubscribeToTransmitter(transmitter3);
        _receivers.Subscribe(_receivedInput.Receive);
        _receivers.processWholeNibble = true;
        _receivedInput.input = SNibble.ZeroValue;

        SNibble nib = new SNibble();
        nib.RawValue = 0b_0001;

        transmitter1.SendSignal(nib);
        transmitter2.SendSignal(nib);

        yield return new WaitForSeconds(0.01f);

        Assert.AreEqual(SNibble.ZeroValue, _receivedInput.input,
                        "Received value should be 0. Receiver should wait for receiving all the signals from transmitters" + _receivedInput.input.RawValue);

        transmitter3.SendSignal(SNibble.ZeroValue);

        yield return new WaitForSeconds(0.01f);

        Assert.AreEqual(nib, _receivedInput.input, "Should receive value.");
    }

    [UnityTest]
    public IEnumerator TestReceiveWholeNibbleMultipleTransmittersReturnsLargerPositiveAbsValue()
    {
        GameObject transmitterObject = new GameObject();
        Transmitter transmitter1 = transmitterObject.AddComponent<Transmitter>();
        Transmitter transmitter2 = transmitterObject.AddComponent<Transmitter>();

        _receiver = _testObject.GetComponentInChildren<Receiver>();
        _receiver.SubscribeToTransmitter(transmitter1);
        _receiver.SubscribeToTransmitter(transmitter2);
        _receivers.Subscribe(_receivedInput.Receive);
        _receivers.processWholeNibble = true;
        _receivedInput.input = SNibble.ZeroValue;

        SNibble nibSmaller = new SNibble();
        SNibble nibLarger = new SNibble();
        nibSmaller.RawValue = 0b_0001;
        nibLarger.RawValue = 0b_0011;

        transmitter1.SendSignal(nibLarger);
        transmitter2.SendSignal(nibSmaller);

        yield return new WaitForSeconds(0.01f);

        Assert.AreEqual(nibLarger, _receivedInput.input,
                        "Received value should be larger positive value" + _receivedInput.input.RawValue);
    }

    [UnityTest]
    public IEnumerator TestReceiveWholeNibbleMultipleTransmittersReturnsLargerNegativeAbsValue()
    {
        GameObject transmitterObject = new GameObject();
        Transmitter transmitter1 = transmitterObject.AddComponent<Transmitter>();
        Transmitter transmitter2 = transmitterObject.AddComponent<Transmitter>();

        _receiver = _testObject.GetComponentInChildren<Receiver>();
        _receiver.SubscribeToTransmitter(transmitter1);
        _receiver.SubscribeToTransmitter(transmitter2);
        _receivers.Subscribe(_receivedInput.Receive);
        _receivers.processWholeNibble = true;
        _receivedInput.input = SNibble.ZeroValue;

        SNibble nibSmaller = new SNibble();
        SNibble nibLarger = new SNibble();
        nibSmaller.RawValue = 0b_1001;
        nibLarger.RawValue = 0b_1011;

        transmitter1.SendSignal(nibSmaller);
        transmitter2.SendSignal(nibLarger);

        yield return new WaitForSeconds(0.01f);

        Assert.AreEqual(nibLarger, _receivedInput.input,
                        "Received value should be larger positive value" + _receivedInput.input.RawValue);
    }

    [UnityTest]
    public IEnumerator TestReceiveWholeNibbleMultipleTransmittersReturnsPositiveOverNegativeAbsValue()
    {
        GameObject transmitterObject = new GameObject();
        Transmitter transmitter1 = transmitterObject.AddComponent<Transmitter>();
        Transmitter transmitter2 = transmitterObject.AddComponent<Transmitter>();

        _receiver = _testObject.GetComponentInChildren<Receiver>();
        _receiver.SubscribeToTransmitter(transmitter1);
        _receiver.SubscribeToTransmitter(transmitter2);
        _receivers.Subscribe(_receivedInput.Receive);
        _receivers.processWholeNibble = true;
        _receivedInput.input = SNibble.ZeroValue;

        transmitter1.SendSignal(SNibble.MaxValue);
        transmitter2.SendSignal(SNibble.MinValue);

        yield return new WaitForSeconds(0.01f);

        Assert.AreEqual(SNibble.MaxValue, _receivedInput.input,
                        "Received value should be larger positive value" + _receivedInput.input.RawValue);
    }
}
