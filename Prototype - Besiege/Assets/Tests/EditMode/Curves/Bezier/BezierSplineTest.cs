using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using static CustomAssert;

public class BezierSplineTest
{
    [Test]
    public void TestStartPoint()
    {
        List<Vector3> points = new List<Vector3>{
            new Vector3(0,0,0),
            new Vector3(0,1,0.5f),
            new Vector3(0,0,1)
        };
        BezierSpline curve = new BezierSpline(points, Vector3.up, Vector3.up);


        CustomAssert.VectorsApproximatelyEqual(points[0], curve.Interpolate(0).position);
    }

    [Test]
    public void TestEndPoint()
    {
        List<Vector3> points = new List<Vector3>{
            new Vector3(0,0,0),
            new Vector3(0,1,0.5f),
            new Vector3(0,0,1)
        };
        BezierSpline curve = new BezierSpline(points, Vector3.up, Vector3.up);

        CustomAssert.VectorsApproximatelyEqual(points[2], curve.Interpolate(1).position);
    }

    [Test]
    public void TestMidPoint()
    {
        List<Vector3> points = new List<Vector3>{
            new Vector3(0,0,0),
            new Vector3(0,1,0.5f),
            new Vector3(0,0,1)
        };
        BezierSpline curve = new BezierSpline(points, Vector3.up, Vector3.up);

       CustomAssert.VectorsApproximatelyEqual(points[1], curve.Interpolate(0.5f).position);
    }

    [Test]
    public void TestStartTangent()
    {
        List<Vector3> points = new List<Vector3>{
            new Vector3(0,0,0),
            new Vector3(0,1,0.5f),
            new Vector3(0,0,1)
        };
        BezierSpline curve = new BezierSpline(points, Vector3.up, Vector3.up);

        CustomAssert.VectorsApproximatelyEqual(Vector3.up, curve.Interpolate(0).tangent.normalized);
    }

    [Test]
    public void TestEndTangent(){
        List<Vector3> points = new List<Vector3>{
            new Vector3(0,0,0),
            new Vector3(0,1,0.5f),
            new Vector3(0,0,1)
        };
        BezierSpline curve = new BezierSpline(points, Vector3.up, Vector3.up);

        CustomAssert.VectorsApproximatelyEqual(Vector3.down, curve.Interpolate(1).tangent.normalized);
    }

    [Test]
    public void TestMidTangent(){
        List<Vector3> points = new List<Vector3>{
            new Vector3(0,0,0),
            new Vector3(0,1,0.5f),
            new Vector3(0,0,1)
        };
        BezierSpline curve = new BezierSpline(points, Vector3.up, Vector3.up);

       CustomAssert.VectorsApproximatelyEqual(new Vector3(0,0,1), curve.Interpolate(0.5f).tangent.normalized);
    }

    [Test]
    public void TestLength(){
        List<Vector3> points = new List<Vector3>{
            new Vector3(0,0,0),
            new Vector3(0,1,0.5f),
            new Vector3(0,0,1)
        };
        BezierSpline curve = new BezierSpline(points, Vector3.up, Vector3.up);

        //length for this particular bezier spline should be between length of lines start->midpoint->endpoint
        //and length of lines created by startpoint->startpoint + Vector3.up->endpoint + Vector3.up->endpoint
        Assert.Greater(curve.Length, 2 * Vector3.Distance(points[1], new Vector3(0,0,0)), curve.Length.ToString("F2"));
        Assert.Less(curve.Length, 3, curve.Length.ToString("F2"));
    }

}
