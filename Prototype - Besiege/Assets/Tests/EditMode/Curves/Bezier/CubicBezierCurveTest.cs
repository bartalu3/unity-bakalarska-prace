using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

public class CubicBezierCurveTest
{

    [Test]
    public void TestStartPoint()
    {
        Vector3 startPoint = new Vector3(0,0,0);
        Vector3 endPoint = new Vector3(0,0,1);
        Vector3 startControlPoint = new Vector3(0,1,0);
        Vector3 endControlPoint = new Vector3(0,1,1);
        CubicBezierCurve curve = new CubicBezierCurve(startPoint, endPoint, startControlPoint, endControlPoint);

        Assert.AreEqual(startPoint, curve.Interpolate(0).position);
    }

    [Test]
    public void TestEndPoint()
    {
        Vector3 startPoint = new Vector3(0,0,0);
        Vector3 endPoint = new Vector3(0,0,1);
        Vector3 startControlPoint = new Vector3(0,1,0);
        Vector3 endControlPoint = new Vector3(0,1,1);
        CubicBezierCurve curve = new CubicBezierCurve(startPoint, endPoint, startControlPoint, endControlPoint);

        Assert.AreEqual(endPoint, curve.Interpolate(1).position);
    }

    [Test]
    public void TestMidPoint()
    {
        Vector3 startPoint = new Vector3(0,0,0);
        Vector3 endPoint = new Vector3(0,0,1);
        Vector3 startControlPoint = new Vector3(0,1,0);
        Vector3 endControlPoint = new Vector3(0,1,1);
        CubicBezierCurve curve = new CubicBezierCurve(startPoint, endPoint, startControlPoint, endControlPoint);

        Assert.AreEqual(new Vector3(0,0.75f,0.5f), curve.Interpolate(0.5f).position);
    }

    [Test]
    public void TestStartTangent()
    {
        Vector3 startPoint = new Vector3(0,0,0);
        Vector3 endPoint = new Vector3(0,0,1);
        Vector3 startControlPoint = new Vector3(0,1,0);
        Vector3 endControlPoint = new Vector3(0,1,1);
        CubicBezierCurve curve = new CubicBezierCurve(startPoint, endPoint, startControlPoint, endControlPoint);

        Assert.AreEqual(new Vector3(0,1,0), curve.Interpolate(0).tangent.normalized);
    }

    [Test]
    public void TestEndTangent(){
        Vector3 startPoint = new Vector3(0,0,0);
        Vector3 endPoint = new Vector3(0,0,1);
        Vector3 startControlPoint = new Vector3(0,1,0);
        Vector3 endControlPoint = new Vector3(0,1,1);
        CubicBezierCurve curve = new CubicBezierCurve(startPoint, endPoint, startControlPoint, endControlPoint);

        Assert.AreEqual(new Vector3(0,-1,0), curve.Interpolate(1).tangent.normalized);
    }

    [Test]
    public void TestMidTangent(){
        Vector3 startPoint = new Vector3(0,0,0);
        Vector3 endPoint = new Vector3(0,0,1);
        Vector3 startControlPoint = new Vector3(0,1,0);
        Vector3 endControlPoint = new Vector3(0,1,1);
        CubicBezierCurve curve = new CubicBezierCurve(startPoint, endPoint, startControlPoint, endControlPoint);

        Assert.AreEqual(new Vector3(0,0,1), curve.Interpolate(0.5f).tangent.normalized);
    }

    [Test]
    public void TestLength(){
        Vector3 startPoint = new Vector3(0,0,0);
        Vector3 endPoint = new Vector3(0,0,1);
        Vector3 startControlPoint = new Vector3(0,1,0);
        Vector3 endControlPoint = new Vector3(0,1,1);
        CubicBezierCurve curve = new CubicBezierCurve(startPoint, endPoint, startControlPoint, endControlPoint);

        //length for this particular bezier curve should be between length of lines start->midpoint->endpoint
        //and length of lines created by startPoint->startControlPoint->endControlPoint->endPoint
        Assert.Greater(curve.Length, 2 * Vector3.Distance(new Vector3(0,0.75f,0.5f), new Vector3(0,0,0)), curve.Length.ToString("F2"));
        Assert.Less(curve.Length, 3, curve.Length.ToString("F2"));
    }

}
