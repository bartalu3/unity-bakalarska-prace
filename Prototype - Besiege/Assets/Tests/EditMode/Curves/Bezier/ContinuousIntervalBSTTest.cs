using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

public class ContinuousIntervalBSTTest
{

    [Test]
    public void TestAtEdgesIntervals()
    {
        //creates intervals <0; 1> (1;2>
        ContinuousIntervalBST intervalTree = new ContinuousIntervalBST();
        intervalTree.AddInterval(1);
        intervalTree.AddInterval(2);

        Assert.AreEqual(0, intervalTree.IndexAt(1));
    }

    [Test]
    public void TestAtZero()
    {
        //creates intervals <0; 1> (1;2>
        ContinuousIntervalBST intervalTree = new ContinuousIntervalBST();
        intervalTree.AddInterval(1);
        intervalTree.AddInterval(2);

        Assert.AreEqual(0, intervalTree.IndexAt(0));
    }

    [Test]
    public void TestAtEnd()
    {
        //creates intervals <0; 1> (1;2>
        ContinuousIntervalBST intervalTree = new ContinuousIntervalBST();
        intervalTree.AddInterval(1);
        intervalTree.AddInterval(2);

        Assert.AreEqual(1, intervalTree.IndexAt(2));
    }

    [Test]
    public void TestAtRandom()
    {
        //creates intervals <0; 1> (1;2>
        ContinuousIntervalBST intervalTree = new ContinuousIntervalBST();
        intervalTree.AddInterval(1);
        intervalTree.AddInterval(2);

        Assert.AreEqual(1, intervalTree.IndexAt(1.9f));
        Assert.AreEqual(0, intervalTree.IndexAt(0.9f));
        Assert.AreEqual(0, intervalTree.IndexAt(0.01f));
        Assert.AreEqual(1, intervalTree.IndexAt(1.01f));
    }

}
