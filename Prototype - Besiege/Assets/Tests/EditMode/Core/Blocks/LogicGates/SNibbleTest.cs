using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

public class SNibbleTest
{

    [Test]
    public void TestEqualsZeroAndNegativeZero()
    {
        SNibble nibNegativeZero = new SNibble();
        nibNegativeZero.RawValue = 0b_1000;

        Assert.AreEqual(nibNegativeZero, SNibble.ZeroValue);
    }

    [Test]
    public void TestEqualsNegative()
    {
        SNibble nibNegative = new SNibble();
        nibNegative.RawValue = 0b_1011;

        Assert.AreEqual(nibNegative, nibNegative);
    }

    [Test]
    public void TestEqualsPositive()
    {
        SNibble nibPositive = new SNibble();
        nibPositive.RawValue = 0b_100;

        Assert.AreEqual(nibPositive, nibPositive);
    }

    [Test]
    public void TestAreNotEqualZeroAndPositive()
    {
        SNibble nib = new SNibble();
        nib.RawValue = 0b_1;

        Assert.AreNotEqual(nib, SNibble.ZeroValue);
    }

    [Test]
    public void TestAreNotEqualZeroAndNegative()
    {
        SNibble nib = new SNibble();
        nib.RawValue = 0b_1011;

        Assert.AreNotEqual(nib, SNibble.ZeroValue);
    }

    [Test]
    public void TestAreNotEqualNegativeAndNegative()
    {
        SNibble nibNegative = new SNibble();
        nibNegative.RawValue = 0b_1011;
        SNibble nibNegativeSmaller = new SNibble();
        nibNegativeSmaller.RawValue = 0b_1001;

        Assert.AreNotEqual(nibNegativeSmaller, nibNegative);
    }

    [Test]
    public void TestAreNotEqualPositiveAndPositive()
    {
        SNibble nibPositive = new SNibble();
        nibPositive.RawValue = 0b_101;
        SNibble nibPositiveSmaller = new SNibble();
        nibPositiveSmaller.RawValue = 0b_001;

        Assert.AreNotEqual(nibPositiveSmaller, nibPositive);
    }

    [Test]
    public void TestAreNotEqualJMaxValueAndMinValue()
    {
        Assert.AreNotEqual(SNibble.MaxValue, SNibble.MinValue);
    }


    [Test]
    public void TestMaxAbsReturnsPositiveZero()
    {
        SNibble nibNegativeZero = new SNibble();
        nibNegativeZero.RawValue = 0b_1000;

        Assert.AreEqual(SNibble.MaxAbs(nibNegativeZero, SNibble.ZeroValue), SNibble.ZeroValue);
    }

    [Test]
    public void TestMaxAbsReturnsPositiveMaxValue()
    {
        SNibble negativeMaxValue = SNibble.MinValue;
        negativeMaxValue.RawValue = 0b_1011;

        Assert.AreEqual(SNibble.MaxAbs(SNibble.MinValue, SNibble.MaxValue), SNibble.MaxValue);
    }

    [Test]
    public void TestMaxAbsReturnsPositiveValue()
    {
        SNibble nibNegative = new SNibble();
        nibNegative.RawValue = 0b_1001;
        SNibble nibPositive = new SNibble();
        nibPositive.RawValue = 0b_0001;

        Assert.AreEqual(SNibble.MaxAbs(nibNegative, nibPositive), nibPositive);
    }

    [Test]
    public void TestMaxAbsReturnsNegativeLargerValue()
    {
        SNibble nibNegative = new SNibble();
        nibNegative.RawValue = 0b_1011;
        SNibble nibPositive = new SNibble();
        nibPositive.RawValue = 0b_0001;

        Assert.AreEqual(SNibble.MaxAbs(nibNegative, nibPositive), nibNegative);
    }

    [Test]
    public void TestMaxAbsReturnsPositiveLargerValue()
    {
        SNibble nibNegative = new SNibble();
        nibNegative.RawValue = 0b_1011;
        SNibble nibPositive = new SNibble();
        nibPositive.RawValue = 0b_0101;

        Assert.AreEqual(SNibble.MaxAbs(nibNegative, nibPositive), nibPositive);
    }

    [Test]
    public void TestMaxAbsReturnsPositiveValueOverZero()
    {
        SNibble nibPositive = new SNibble();
        nibPositive.RawValue = 0b_0101;

        Assert.AreEqual(SNibble.MaxAbs(SNibble.ZeroValue, nibPositive), nibPositive);
    }

    [Test]
    public void TestMaxAbsReturnsPositiveValueOverNegativeZero()
    {
        SNibble nibNegativeZero = new SNibble();
        nibNegativeZero.RawValue = 0b_1000;
        SNibble nibPositive = new SNibble();
        nibPositive.RawValue = 0b_0101;

        Assert.AreEqual(SNibble.MaxAbs(nibPositive, nibNegativeZero), nibPositive);
    }

    [Test]
    public void TestMaxAbsReturnsNegativeValueOverZero()
    {
        SNibble nibNegative = new SNibble();
        nibNegative.RawValue = 0b_1001;

        Assert.AreEqual(SNibble.MaxAbs(SNibble.ZeroValue, nibNegative), nibNegative);
    }

    [Test]
    public void TestMaxAbsReturnsNegativeValueOverNegativeZero()
    {
        SNibble nibNegativeZero = new SNibble();
        nibNegativeZero.RawValue = 0b_1000;
        SNibble nibNegative = new SNibble();
        nibNegative.RawValue = 0b_1010;

        Assert.AreEqual(SNibble.MaxAbs(nibNegative, nibNegativeZero), nibNegative);
    }



}
