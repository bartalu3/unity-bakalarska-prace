using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using static CustomAssert;

public class MeshCircleTest
{
    [Test]
    public void TestPointsAreOnCircle()
    {
        MeshCircle circle = new MeshCircle(new Vector3(0,0,0), Quaternion.identity, 1f, 3);
        List<Vector3> points = circle.GetPoints();
        Assert.AreEqual(new Vector3(0,0,1), points[0]);
        Assert.Less(Vector3.Distance(new Vector3(0.866f,0,-0.5f), points[1]), 0.001f);
        Assert.Less(Vector3.Distance(new Vector3(-0.866f,0,-0.5f), points[2]), 0.001f);
    }

    [Test]
    public void TestPositionOfCircle(){
        MeshCircle circle = new MeshCircle(new Vector3(1,1,1), Quaternion.identity, 1f, 3);
        List<Vector3> points = circle.GetPoints();
        Assert.AreEqual(new Vector3(0,0,1) + new Vector3(1,1,1), points[0]);
    }

    [Test]
    public void TestRotationOfCircle(){
        Quaternion rotation = Quaternion.Euler(90, 0, 0);
        MeshCircle circle = new MeshCircle(new Vector3(1,1,1), rotation, 1f, 3);
        List<Vector3> points = circle.GetPoints();
        CustomAssert.VectorsApproximatelyEqual(new Vector3(1,0,1), points[0], 0.001f);
    }

}
