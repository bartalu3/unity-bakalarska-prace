using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NUnit.Framework;

public class CustomAssert
{
    public static void VectorsApproximatelyEqual(Vector3 expected, Vector3 actual, float tolerance = 0.001f){
        if(Vector3.Distance(expected, actual) > tolerance){
            string message = "Expected: " + expected.ToString("F2") + "\n---\nActual: " + actual.ToString("F2");
            Assert.Fail(message);
        }
    }
}
