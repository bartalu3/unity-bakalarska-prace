using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TempButtonGameModeHandlerer : MonoBehaviour
{
    public bool BuildModeClick;
    public bool BuildMode;
    public void Start(){
        BuildMode = false;
    }

    public void UpdateBuildMode(){
        if (BuildMode){
            BuildMode = false;
            BuildModeChange.TurnBuildModeOn();
        }
        else{
            BuildMode = true;
            BuildModeChange.TurnBuildModeOff();
        }
    }

    private void Update() {
        if(BuildModeClick){
            BuildModeClick = false;
            UpdateBuildMode();
        }
    }
}
