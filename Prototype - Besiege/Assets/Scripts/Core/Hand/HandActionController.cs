using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;
public class HandActionController : MonoBehaviour
{

    private ActionBasedController _controller;
    private GrabController _grabController;
    private GripController _gripController;
    // Start is called before the first frame update
    void Start(){
        _controller = GetComponent<ActionBasedController>();
        _grabController = GetComponentInChildren<GrabController>();
        _gripController = GetComponentInChildren<GripController>();
        _controller.activateAction.action.performed += _grabController.Grab;
        _controller.activateAction.action.canceled += _grabController.Release;
        _controller.selectAction.action.performed += _gripController.Grip;
        _controller.selectAction.action.canceled += _gripController.GripRelease;
    }

    private void OnValidate() {
        if(GetComponentInChildren<GrabController>() == null){
            Debug.LogWarning("Missing grab controller");
        }
        if(GetComponentInChildren<GripController>() == null){
            Debug.LogWarning("Missing grip controller");
        }
    }
}
