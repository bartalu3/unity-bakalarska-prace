using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GripController : MonoBehaviour
{
    public GameObject OtherHand;
    public GameObject Hand;
    public GameObject XRCamera;

    private GripController _otherHandGripController;
    private Transform _rigTranform;
    private Transform _otherHandTransform;
    private Vector3 _anchorLocalPosition;
    private Vector3 _moveRigStartPosition;
    private ObjectsInTriggerArea _grabableObjects;

    private float _defaultScale = 5f;
    private float _minScale = 1f;
    private float _maxScale = 15f;
    private float _scaleModeStartHandDistance;
    private float _startingScale;

    private bool _gripPressed = false;
    private bool _movableTreeMode = false;
    private bool _scaleMode = false;
    private bool _skipUpdate = false;

    private void Start(){
        _rigTranform = gameObject.transform.root.gameObject.transform;
        _grabableObjects = GetComponent<ObjectsInTriggerArea>();
        _otherHandTransform = OtherHand.transform;
        _otherHandGripController = OtherHand.GetComponentInChildren<GripController>();
    }

    private void FixedUpdate() {
        if(_skipUpdate){
            return;
        }
        if(_scaleMode){
            ScaleXRRig();
        }
        else if(_gripPressed){
            MoveXRRig();

        }
    }

    public void Grip(UnityEngine.InputSystem.InputAction.CallbackContext obj){
        _anchorLocalPosition = Hand.transform.localPosition;
        _moveRigStartPosition = _rigTranform.position;
        _gripPressed = true;
        if(_otherHandGripController.OtherHandGripped(_grabableObjects.GetClosest())){
            _skipUpdate = true;
        }
    }

    public void GripRelease(UnityEngine.InputSystem.InputAction.CallbackContext obj){
        _gripPressed = false;
        _movableTreeMode = false;
        _scaleMode = false;
        _skipUpdate = false;
        _otherHandGripController.OtherHandReleased();
    }

    public bool OtherHandGripped(GameObject grabbableInOtherHand){
        GameObject closestGrabable = _grabableObjects.GetClosest();
        if(_gripPressed && grabbableInOtherHand != null && closestGrabable != null){
            // move movable tree of blocks
        }
        else if (_gripPressed){
            _scaleMode = true;
            _anchorLocalPosition = Hand.transform.localPosition;
            _scaleModeStartHandDistance = Vector3.Distance(_anchorLocalPosition, _otherHandTransform.localPosition);
            _startingScale = _rigTranform.localScale.x;
            }
        return _gripPressed;
    }

    public void OtherHandReleased(){
        _moveRigStartPosition = _rigTranform.position;
        _anchorLocalPosition = Hand.transform.localPosition;
        _skipUpdate = false;
        _movableTreeMode = false;
        _scaleMode = false;
    }

    private void ScaleXRRig(){
        Vector3 cameraPosition = XRCamera.transform.position;
        float currentDistance = Vector3.Distance(Hand.transform.localPosition, _otherHandTransform.localPosition);
        float scale = currentDistance / _scaleModeStartHandDistance;
        float newScale = _startingScale * scale;
        float scaleDiff;
        if(newScale > _maxScale){
            scaleDiff = _rigTranform.localScale.x - _maxScale;
            _rigTranform.localScale = new Vector3(_maxScale, _maxScale, _maxScale);
        }
        else if(newScale < _minScale){
            scaleDiff =_rigTranform.localScale.x - _minScale;
            _rigTranform.localScale = new Vector3(_minScale, _minScale, _minScale);
        }
        else{
            scaleDiff =_rigTranform.localScale.x - newScale;
            _rigTranform.localScale = new Vector3(newScale, newScale, newScale);
        }

        Vector3 cameraLookDirection = XRCamera.transform.forward;
        Vector3 cameraOffset = cameraPosition - XRCamera.transform.position + cameraLookDirection * 0.4f * scaleDiff;
        _rigTranform.position = _rigTranform.position + cameraOffset;
    }

    private void MoveXRRig(){
        Vector3 positionOffset = (_anchorLocalPosition - Hand.transform.localPosition) * 2f * _rigTranform.localScale.x;
        _rigTranform.position = _moveRigStartPosition + positionOffset;
    }
}
