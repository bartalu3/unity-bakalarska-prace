using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[SelectionBase]
[RequireComponent(typeof(ObjectsInTriggerArea))]
public class TestHand : BuildModeObserver
{
    [SerializeField]
    public bool fakeClick = false;
    public bool fakeRelease = false;

    private GameObject _grabbedObject = null;
    private Quaternion _grabbedObjectRotation;
    private Grabbable _grabbableComponent;

    private ObjectsInTriggerArea _grabableObjects;

    private void Start() {
        _grabableObjects = GetComponent<ObjectsInTriggerArea>();
    }

    void Update()
    {
        if (fakeClick){
            fakeClick = false;
            Grab();
        }
        if (fakeRelease){
            fakeRelease = false;
            Release();
        }
    }

    private void FixedUpdate() {
        if(_grabbedObject != null){
            Vector3 offset = gameObject.transform.rotation * Vector3.forward;
            _grabbedObject.transform.position = gameObject.transform.position + offset;
            _grabbedObject.transform.rotation = _grabbedObjectRotation * gameObject.transform.rotation;
        }
    }

    private void Grab(){
        _grabbedObject = _grabableObjects.GetClosest();
        if(_grabbedObject != null){
            _grabbableComponent = _grabbedObject.GetComponentInChildren<Grabbable>();
            _grabbedObject = _grabbableComponent.Grab();
            _grabbedObject.tag = "Grabbed";
            _grabbableComponent = _grabbedObject.GetComponentInChildren<Grabbable>();
            _grabbedObjectRotation = _grabbedObject.transform.rotation;
        }
    }

    private void Release(){
        if (_grabbedObject != null){
            _grabbedObject.tag = "Grabbable";
            _grabbedObject = null;
            _grabbableComponent.Release();
        }
    }

    protected override void BuildModeTurnedOff(){
        if (_grabbedObject != null){
            _grabbableComponent.ForceRelease();
        }
    }

    protected override void BuildModeTurnedOn(){
        //TODO: cant grab
    }

}
