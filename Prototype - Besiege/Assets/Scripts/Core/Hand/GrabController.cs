using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[SelectionBase]
[RequireComponent(typeof(ObjectsInTriggerArea))]
public class GrabController : BuildModeObserver
{
    private GameObject _grabbedObject = null;
    private Quaternion _grabbedObjectInitialRotation;
    private Quaternion _handInitialInverseRotation;
    private Vector3 _grabbedObjectOffset;
    private Grabbable _grabbableComponent;

    private ObjectsInTriggerArea _grabableObjects;

    private void Start() {
        _grabableObjects = GetComponent<ObjectsInTriggerArea>();
    }

    private void FixedUpdate() {
        if(_grabbedObject != null){
            SolveGrabbedObjectRotation();
        }
    }

    public void Grab(UnityEngine.InputSystem.InputAction.CallbackContext obj){
        _grabbedObject = _grabableObjects.GetClosest();
        if(_grabbedObject != null){
            _grabbableComponent = _grabbedObject.GetComponentInChildren<Grabbable>();
            _grabbedObject = _grabbableComponent.Grab();
            _grabbedObject.tag = "Grabbed";
            _grabbableComponent = _grabbedObject.GetComponentInChildren<Grabbable>();
            _grabbedObjectInitialRotation = _grabbedObject.transform.rotation;
            _handInitialInverseRotation = Quaternion.Inverse(gameObject.transform.rotation);
            _grabbedObjectOffset = gameObject.transform.InverseTransformPoint(_grabbedObject.transform.position);
        }
    }

    public void Release(UnityEngine.InputSystem.InputAction.CallbackContext obj){
        if (_grabbedObject != null){
            _grabbedObject.tag = "Grabbable";
            _grabbedObject = null;
            _grabbableComponent.Release();
        }
    }

    protected override void BuildModeTurnedOff(){
        if (_grabbedObject != null){
            _grabbableComponent.ForceRelease();
        }
    }

    protected override void BuildModeTurnedOn(){
        //TODO: cant grab
    }

    private void SolveGrabbedObjectRotation(){
        _grabbedObject.transform.position = gameObject.transform.TransformPoint(_grabbedObjectOffset);
        _grabbedObject.transform.rotation = (gameObject.transform.rotation * _handInitialInverseRotation) * _grabbedObjectInitialRotation;
    }

}
