using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(NibbleInput))]
[RequireComponent(typeof(HingeJoint))]
public class PoweredWheelController : MonoBehaviour
{
    [SerializeField]
    public float maxSpeed;
    private NibbleInput _inputComponent;
    private HingeJoint _hingeComponent;
    private JointMotor _motor;

    private const float NON_ZERO_SPEED_OFFSET = 0.0001f;

    private void Start() {
        _inputComponent = GetComponent<NibbleInput>();
        _hingeComponent = GetComponent<HingeJoint>();
        _motor = _hingeComponent.motor;
    }

    private void FixedUpdate() {
        SetMotorSpeed();
    }

    private void SetMotorSpeed(){
        SNibble nib = _inputComponent.Input;
        float speedScale = nib.SignedByte / 7f;
        _motor.targetVelocity = speedScale * maxSpeed + NON_ZERO_SPEED_OFFSET;
        _hingeComponent.motor = _motor;
    }
}
