using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[SelectionBase]
public class GrabbableConnector : Grabbable
{

    [SerializeField]
    public bool grabbed = false;
    public bool click = false;
    public bool release = false;
    public GameObject cablePrefab;

    public override GameObject Grab(){
        GameObject cable = Instantiate(cablePrefab, transform.position, transform.rotation);
        CableController cableController = cable.GetComponent<CableController>();
        GameObject cableEndpoint = cableController.GetCableEndpoint();
        cableController.BindTransmitter(GetComponent<Transmitter>());
        return cableEndpoint;
    }
    public override void Release(){}

    public override void ForceRelease(){}
}

