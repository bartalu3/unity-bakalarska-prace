using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Transmitter : MonoBehaviour
{
    public HashSet<GameObject> cables = new HashSet<GameObject>();
    private event Action<SNibble> _observers;

    public void Subscribe(Action<SNibble> receiveSignal) {
        _observers += receiveSignal;
    }

    public void Unsubscribe(Action<SNibble> receiveSignal) {
        _observers -= receiveSignal;
    }

    public void SendSignal(SNibble signal) {
        _observers?.Invoke(signal);
    }
}
