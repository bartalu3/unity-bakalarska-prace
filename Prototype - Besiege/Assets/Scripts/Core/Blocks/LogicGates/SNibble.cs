using System;

[System.Serializable]
public struct SNibble
{
    public byte _value;

    public byte RawValue{
        set{
            if(value > 15){
                throw new ArgumentOutOfRangeException("Nibble supports only values from 0 to 15");
            }
            _value = value;
        }
        get {return _value;}
    }

    public sbyte SignedByte{
        get{
            sbyte signedByte;
            if (_value > 7){
                signedByte = (sbyte)((_value - 8) * -1);
            }
            else{
                signedByte = (sbyte)_value;
            }
            return signedByte;
        }
    }

    public static SNibble MaxValue{
        get {
            SNibble nib = new SNibble();
            nib.RawValue = 0b_111;
            return nib;
        }
    }

    public static SNibble ZeroValue{
        get {
            SNibble nib = new SNibble();
            nib.RawValue = 0b_0;
            return nib;
        }
    }

    public static SNibble MinValue{
        get {
            SNibble nib = new SNibble();
            nib.RawValue = 0b_1111;
            return nib;
        }
    }

    public bool BitValueAt(int index){
        if(index > 3 && index < 0){
            throw new ArgumentOutOfRangeException("Maximum nibble index is 3");
        }
        byte mask = (byte)(1 << index);
        return 0 < (mask & _value);
    }

    public static SNibble MaxAbs(SNibble x, SNibble y){
        SNibble absX = x;
        SNibble absY = y;

        if(absX._value >= 8){
            absX._value -= 0b_1000;
        }
        if(absY._value >= 8){
            absY._value -= 0b_1000;
        }

        //prefers positive value over negative
        if(absX._value == absY._value){
            if(x._value < 8){
                return x;
            }
        }

        if(absX._value > absY._value){
            return x;
        }
        return y;
    }

    public static  bool operator ==(SNibble x, SNibble y){
        if((x.RawValue == 0 && y.RawValue == 8) || (x.RawValue == 8 && y.RawValue == 0)){
            return true;
        }
        return x.RawValue == y.RawValue;
    }

    public static  bool operator !=(SNibble x, SNibble y) => !(x == y);

    public override bool Equals(System.Object obj) {
        return this == (SNibble)obj;
    }

    public override int GetHashCode() {
        return _value.GetHashCode();
    }


}

