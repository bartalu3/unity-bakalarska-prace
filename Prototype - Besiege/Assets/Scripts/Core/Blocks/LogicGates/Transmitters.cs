using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Transmitters : LogicGateIO
{
    private List<Transmitter> _cachedTransmitters = new List<Transmitter>();

    public void SendSignal(SNibble signal) {
        if(_cachedTransmitters.Count == 0){
            return;
        }
        if(processWholeNibble) {
            _cachedTransmitters[0].SendSignal(signal);
            return;
        }
        for(int i = 0; i < _cachedTransmitters.Count; i++) {
            if(signal.BitValueAt(i)) {
                _cachedTransmitters[i].SendSignal(SNibble.MaxValue);
            }
            else {
                _cachedTransmitters[i].SendSignal(SNibble.ZeroValue);
            }
        }
    }

    public List<GameObject> Cables{
        get{
            List<GameObject> cables = new List<GameObject>();
            foreach(Transmitter transmitter in _cachedTransmitters){
                foreach(GameObject cable in transmitter.cables){
                    cables.Add(cable);
                }
            }
            return cables;
        }
    }

    protected override void Start() {
        base.Start();
        foreach(GameObject transmitterObject in connectorObjects) {
            Transmitter transmitterComponent = transmitterObject.GetComponent<Transmitter>();
            _cachedTransmitters.Add(transmitterComponent);
        }
    }

    protected override Color GizmoColor(){
        return Color.magenta;
    }
}
