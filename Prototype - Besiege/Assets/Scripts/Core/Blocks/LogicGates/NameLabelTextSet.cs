using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class NameLabelTextSet : MonoBehaviour
{
    void Start()
    {
        TextMeshPro textBoxes = GetComponent<TextMeshPro>();
        textBoxes.text = FormatName(transform.root.gameObject.name);

    }

    private string FormatName(string name){
        string formatedName = "";
        if(name == ""){
            return "--";
        }
        for(int i = 0; i < (name.Length - 1); i++){
            char letter = name[i];
            if(letter != ' '){
                formatedName = formatedName + letter;
            }

            if(char.IsUpper(letter)){
                continue;
            }
            if(char.IsUpper(name[i + 1])){
                formatedName = formatedName + ' ';
            }
            if(name[i + 1] == '('){
                // Debug.Log("Original name: " + name + "   Foramted name: " + formatedName);
                return formatedName;
            }
        }
        return formatedName + name[name.Length - 1];
    }

}
