using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Receivers : LogicGateIO
{
    private event Action<SNibble> _observers;
    private List<Receiver> _cachedReceivers = new List<Receiver>();
    private int _receivedSignalCounter = 0;

    protected Color gizmosColor = Color.red;

    public void Subscribe(Action<SNibble> receiveSignal){
        _observers += receiveSignal;
    }

    public List<GameObject> Cables{
        get{
            List<GameObject> cables = new List<GameObject>();
            foreach(Receiver receiver in _cachedReceivers){
                foreach(GameObject cable in receiver.cables){
                    cables.Add(cable);
                }
            }
            return cables;
        }
    }

    protected override void Start() {
        base.Start();
        foreach(GameObject receiverObject in connectorObjects) {
            Receiver receiverComponent = receiverObject.GetComponent<Receiver>();
            _cachedReceivers.Add(receiverComponent);
            receiverComponent.Subscribe(ReceivedSignal);
        }
    }

    private void ReceivedSignal(){
        _receivedSignalCounter++;
        if(_receivedSignalCounter == numberOfConnectors){
            _observers?.Invoke(BuildInputNibble());
            _receivedSignalCounter = 0;
        }
    }

    private SNibble BuildInputNibble(){
        if(processWholeNibble){
            return _cachedReceivers[0].ReceivedSignal;
        }
        byte mask = 1;
        SNibble nib = SNibble.ZeroValue;
        for(int i = 0; i < _cachedReceivers.Count; i++) {
            if(_cachedReceivers[i].ReceivedSignal != SNibble.ZeroValue) {
                nib.RawValue += mask;
            }
            mask = (byte)(mask << 1);
        }
        return nib;
    }

    protected override Color GizmoColor(){
        return Color.blue;
    }
}
