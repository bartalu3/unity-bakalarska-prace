using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PushableButtonBuildModeLock : BuildModeObserver
{
    Rigidbody _rigidbody;
    void Start() {
        _rigidbody = GetComponent<Rigidbody>();
    }

    protected override void BuildModeTurnedOn(){
        _rigidbody.isKinematic = false;
    }

    protected override void BuildModeTurnedOff(){
        _rigidbody.isKinematic = true;
    }
}
