using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class LogicGateIO : MonoBehaviour
{
    [SerializeField]
    public GameObject connectorPrefab;
    [Tooltip("It will accept and process whole nibble instead of processing it per bit (whole nibble goes to one connector)")]
    public bool processWholeNibble = false;
    [Range(0, 4)]
    public int numberOfConnectors = 2;
    public float distanceBetweenConnectors = 0.48f;

    protected List<GameObject> connectorObjects = new List<GameObject>();

    protected virtual void Start(){
        CreateConnectors();
    }

    protected void CreateConnectors(){
        float connectorOffset = - ((numberOfConnectors - 1) * distanceBetweenConnectors) / 2;
        for(int i = 0; i < numberOfConnectors; i++){
            Vector3 connectorPosition = new Vector3(connectorOffset + i * distanceBetweenConnectors, 0, 0);
            GameObject connector = Instantiate(connectorPrefab, new Vector3(0,0,0), Quaternion.identity);
            connector.transform.SetParent(this.transform);
            connector.transform.localRotation =  Quaternion.identity;
            connector.transform.localPosition = connectorPosition;
            TextMeshPro textBox = connector.GetComponentInChildren<TextMeshPro>();
            if(processWholeNibble == false && i != 3){
                textBox.text = ((int)Mathf.Pow(2, (float)i)).ToString();
            }
            else if(i == 3){
                textBox.text = "-";
            }
            connectorObjects.Add(connector);
        }
    }

    private void OnValidate() {
        if (processWholeNibble){
            numberOfConnectors = 1;
        }
    }

    private void OnDrawGizmosSelected() {
        Gizmos.color = GizmoColor();
        float connectorOffset = - ((numberOfConnectors - 1) * distanceBetweenConnectors) / 2;
        for(int i = 0; i < numberOfConnectors; i++){
            Vector3 connectorPosition = new Vector3(connectorOffset + i * distanceBetweenConnectors, 0, 0);
            Gizmos.DrawWireSphere(transform.TransformPoint(connectorPosition), 0.15f);
        }
    }

    protected virtual Color GizmoColor(){
        return Color.gray;
    }

}
