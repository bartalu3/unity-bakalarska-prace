using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(MeshFilter))]
public class CableMeshGenerator : MonoBehaviour
{
    [SerializeField]
    public int circleSegments;
    public float cableThickness;
    public float cableResolution;

    private List<Vector3> _points = new List<Vector3>();
    private Vector3 _startDirection;
    private Vector3 _endDirection;
    private bool _removedPointFromList = false;
    private Vector3 _previousPointBeforeRemoving;
    private bool _snapped = false;
    private bool _continuousDrawing = true;
    private ExtrudeAlongAsync _meshExtruderAsync;

    private static float MINIMUM_DISTANCE_FROM_CONNECTOR_INPUT = 2f;

    void Start(){
        _meshExtruderAsync = new ExtrudeAlongAsync();
        _startDirection = (transform.InverseTransformDirection(transform.forward)).normalized;
        _points.Add(transform.InverseTransformPoint(transform.position));
        _points.Add(transform.InverseTransformPoint(transform.position + _startDirection));
    }

    void Update() {
        if(_continuousDrawing){
            DrawAsync();
        }
    }

    public void DrawAsync(){
        if(_points.Count < 2){
            return;
        }
        if(_snapped == false){
            _endDirection = (_points[_points.Count - 2] - _points[_points.Count - 1]).normalized;
        }
        BezierSpline spline = new BezierSpline(_points, _startDirection, _endDirection);
        MeshCircle circle = new MeshCircle(new Vector3(0,0,0), Quaternion.identity, cableThickness, circleSegments);
        GetComponent<MeshFilter>().mesh = _meshExtruderAsync.GenerateMesh(circle, spline, cableResolution);
    }

    public void GenerateFinalMesh(){
        _continuousDrawing = false;
        BezierSpline spline = new BezierSpline(_points, _startDirection, _endDirection);
        MeshCircle circle = new MeshCircle(new Vector3(0,0,0), Quaternion.identity, cableThickness, circleSegments);
        GetComponent<MeshFilter>().mesh = new ExtrudeAlong().GenerateMesh(circle, spline, cableResolution);
    }

    public void UpdateLastPoint(Vector3 point){
        _points[_points.Count - 1] = point;
        _snapped = false;
    }

    public void AddPoint(Vector3 point){
        _points.Add(point);
    }

    public Vector3 GetPreviousPoint(){
        if(_points.Count < 2){
            return Vector3.zero;
        }
        return _points[_points.Count - 2];
    }

    public void Snap(Vector3 point, Vector3 directionOfConnector){
        if(_points.Count > 2 && Vector3.Distance(point, _points[_points.Count - 2]) < MINIMUM_DISTANCE_FROM_CONNECTOR_INPUT ){
            _previousPointBeforeRemoving = _points[_points.Count - 2];
            _points.RemoveAt(_points.Count - 1);
            _removedPointFromList = true;

            UpdateLastPoint(point);
            _endDirection = directionOfConnector;
            _snapped = true;
        }
        else{
            UpdateLastPoint(point);
            _endDirection = directionOfConnector;
            _snapped = true;
        }
    }

    public void Unsnap(){
        if(_removedPointFromList){
            UpdateLastPoint(_previousPointBeforeRemoving);
            _removedPointFromList = false;
        }
        _snapped = false;
    }

}
