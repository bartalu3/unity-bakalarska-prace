using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[RequireComponent(typeof(ObjectsInTriggerArea))]
public class CableEndpointController : MonoBehaviour
{
    private CableController _parentCableController;
    private ObjectsInTriggerArea _connectorsInTriggerArea;

    public void SetParentCableController(CableController controller){
        _parentCableController = controller;
        _connectorsInTriggerArea = GetComponent<ObjectsInTriggerArea>();
    }

    public GameObject GetClosestConnector(){
        return _connectorsInTriggerArea.GetClosest();
    }

    public void Released(){
        _parentCableController.TryToSnapOnInputConnector(_connectorsInTriggerArea.GetClosest());
        Destroy(gameObject);
    }

}
