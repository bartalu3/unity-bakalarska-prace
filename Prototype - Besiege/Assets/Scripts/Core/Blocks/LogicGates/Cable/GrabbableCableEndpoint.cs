using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[SelectionBase]
public class GrabbableCableEndpoint : Grabbable
{
    private CableEndpointController _controller;

    private void Start() {
        _controller = GetComponent<CableEndpointController>();
    }

    public override GameObject Grab(){return null;}

    public override void Release(){
        _controller.Released();
    }

    public override void ForceRelease(){
        Release();
    }
}


