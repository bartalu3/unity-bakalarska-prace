using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(CableMeshGenerator))]
public class CableController : BuildModeObserver
{
    public GameObject cableEndpointPrefab;
    public float controllPointMaxDistance = 2.5f;
    private GameObject _cableEndpoint;
    private CableEndpointController _endpointController;
    private CableMeshGenerator _meshGenetator;
    private MeshRenderer _meshRenderer;

    private Transmitter _transmitter;
    private Receiver _receiver;


    private void Start() {
        _meshGenetator = GetComponent<CableMeshGenerator>();
        _meshRenderer = GetComponent<MeshRenderer>();
    }

    private void OnDestroy() {
        if(_transmitter != null){
            _transmitter.cables.Remove(gameObject);
        }
        if(_receiver != null){
            _receiver.cables.Remove(gameObject);
            _receiver.UnSubscribeFromTransmitter(_transmitter);
        }
    }

    private void FixedUpdate() {
        if(_cableEndpoint == null){
            return;
        }
        CalculateNewControllPoint();
    }

    public Receiver GetReceiver{
        get{return _receiver;}
    }

    public void TryToSnapOnInputConnector(GameObject connector){
        if(connector == null || CreatesCycle(connector)){
            Destroy(gameObject);
            return;
        }
        Vector3 positionLocal = transform.InverseTransformPoint(connector.transform.position);
        _meshGenetator.Snap(positionLocal, DirectionOfControllPoint(connector));
        _meshGenetator.GenerateFinalMesh();
        _cableEndpoint = null;

        BindReceiver(connector.GetComponent<Receiver>());
    }

    public GameObject GetCableEndpoint(){
        if(_cableEndpoint == null){
            _cableEndpoint = Instantiate(cableEndpointPrefab, transform.position, Quaternion.identity);
            _endpointController = _cableEndpoint.GetComponent<CableEndpointController>();
            _endpointController.SetParentCableController(this);
        }
        return _cableEndpoint;
    }

    public void BindTransmitter(Transmitter transmitter){
        _transmitter = transmitter;
        _transmitter.cables.Add(gameObject);
    }

    protected override void BuildModeTurnedOn(){
        _meshRenderer.enabled = true;
    }

    protected override void BuildModeTurnedOff(){
        _meshRenderer.enabled = false;
    }

    private void BindReceiver(Receiver receiver){
        _receiver = receiver;
        _receiver.cables.Add(gameObject);
        _receiver.SubscribeToTransmitter(_transmitter);
    }

    private void CalculateNewControllPoint(){
        GameObject connector = _endpointController.GetClosestConnector();
        if(connector != null && CreatesCycle(connector) == false){
            Vector3 positionLocal = transform.InverseTransformPoint(connector.transform.position);
            _meshGenetator.Snap(positionLocal, DirectionOfControllPoint(connector));
        }
        else{
            _meshGenetator.Unsnap();
            Vector3 positionLocal = transform.InverseTransformPoint(_cableEndpoint.transform.position);
            float controllPointDistance = Vector3.Distance(positionLocal, _meshGenetator.GetPreviousPoint());
            if(controllPointDistance > controllPointMaxDistance){
                _meshGenetator.AddPoint(positionLocal);
            }
            else{
                _meshGenetator.UpdateLastPoint(positionLocal);
            }
        }
    }

    private Vector3 DirectionOfControllPoint(GameObject connector){
        Vector3 directionOfConnector = transform.InverseTransformDirection(connector.transform.forward);
        return directionOfConnector.normalized;
    }

    private bool CreatesCycle(GameObject targetConnector){
        HashSet<GameObject> visited = new HashSet<GameObject>();
        Queue<GameObject> toBeVisited = new Queue<GameObject>();
        toBeVisited.Enqueue(targetConnector.transform.root.gameObject);
        GameObject start = _transmitter.transform.root.gameObject;
        while(toBeVisited.Count != 0){
            GameObject current = toBeVisited.Dequeue();
            if(current == start){
                return true;
            }
            Transmitters[] transmitters = current.GetComponentsInChildren<Transmitters>();
            foreach (Transmitters t in transmitters){
                foreach (GameObject cable in t.Cables){
                    Receiver receiver = cable.GetComponent<CableController>().GetReceiver;
                    GameObject logicObject = receiver.transform.root.gameObject;
                    if(!visited.Contains(logicObject)){
                        toBeVisited.Enqueue(logicObject);
                    }
                    if(logicObject == start){
                        return true;
                    }
                }
            }
            visited.Add(current);
        }
        return false;
    }
}
