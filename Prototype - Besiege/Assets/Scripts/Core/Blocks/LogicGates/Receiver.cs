using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Receiver : MonoBehaviour
{
    private event Action _observers;
    private List<Transmitter> _transmitters = new List<Transmitter>();
    private int _receivedSignalCount = 0;
    private SNibble _receivedSignal;
    public HashSet<GameObject> cables  = new HashSet<GameObject>();


    public void Subscribe(Action notificationCompleteInput){
        _observers += notificationCompleteInput;
    }

    public bool SubscribeToTransmitter(Transmitter transmitter){
        if(_transmitters.Contains(transmitter)){
            return false;
        }
        transmitter.Subscribe(ReceiveSignal);
        _transmitters.Add(transmitter);
        return true;
    }

    public void UnSubscribeFromTransmitter(Transmitter transmitter){
        if(_transmitters.Contains(transmitter)){
            transmitter.Unsubscribe(ReceiveSignal);
            _transmitters.Remove(transmitter);
        }
    }

    public SNibble ReceivedSignal{
        get {return _receivedSignal;}
    }

    private void FixedUpdate() {
        if (_transmitters.Count == 0){
            _receivedSignal = SNibble.ZeroValue;
            _observers?.Invoke();
        }
    }

    private void ReceiveSignal(SNibble signal){
        if(_receivedSignalCount == 0){
            _receivedSignal = SNibble.ZeroValue;
        }
        _receivedSignal = SNibble.MaxAbs(_receivedSignal, signal);
        _receivedSignalCount++;
        NotifyIfReceivedEverything();
    }

    private void NotifyIfReceivedEverything(){
        if(_receivedSignalCount == _transmitters.Count){
            _observers?.Invoke();
            _receivedSignalCount = 0;
        }
    }

    private void OnDrawGizmos() {
        foreach(Transmitter transmitter in _transmitters){
            if(_receivedSignal == SNibble.ZeroValue){
                Gizmos.color = Color.black;
            }
            else if (_receivedSignal.SignedByte > 0){
                Gizmos.color = Color.green;
            }
            else{
                 Gizmos.color = Color.red;
            }
            Gizmos.DrawLine(gameObject.transform.position, transmitter.transform.position);
        }
    }

}
