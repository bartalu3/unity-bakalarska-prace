using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[SelectionBase]
[System.Serializable]
public class LogicGateCore : MonoBehaviour
{
    [SerializeField]
    public List<SNibble> truthTable = new List<SNibble>();

    private Receivers _receivers;
    private Transmitters _transmitters;

    private void Start() {
        _receivers = GetComponentInChildren<Receivers>();
        _transmitters = GetComponentInChildren<Transmitters>();
        _receivers.Subscribe(ProcessInput);
    }

    private void ProcessInput(SNibble nib){
        SNibble signal = truthTable[nib.RawValue];
        _transmitters.SendSignal(signal);
    }


}
