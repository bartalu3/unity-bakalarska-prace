using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PushableButtonController : MonoBehaviour
{

    private float _buttonThreshold = -0.10f;
    private float _buttonDeadZone = 0.010f;

    public GameObject PushableButton;
    public GameObject OnButtonModel;
    public GameObject OffButtonModel;

    private Transmitter _transmitter;
    private Receiver _receiver;

    public bool _ButtonIsOn = false;
    private bool _ButtonIsPressed = false;


    void Start()
    {
        _transmitter = GetComponent<Transmitter>();
        _receiver = GetComponent<Receiver>();
        _receiver.SubscribeToTransmitter(_transmitter);
    }

    private void FixedUpdate(){
        if(_ButtonIsOn){
            _transmitter.SendSignal(SNibble.MaxValue);
        }
        else{
            _transmitter.SendSignal(SNibble.ZeroValue);
        }
        if(!_ButtonIsPressed && GetButtonPosition() <= _buttonThreshold){
            SwitchState();
            _ButtonIsPressed = true;
        }
        else if(_ButtonIsPressed && GetButtonPosition() > _buttonThreshold + _buttonDeadZone){
            _ButtonIsPressed = false;
        }

    }

    public float GetButtonPosition(){
        return PushableButton.transform.localPosition.z;
    }

    public void SwitchState(){
        _ButtonIsOn = !_ButtonIsOn;
        OnButtonModel.SetActive(_ButtonIsOn);
        OffButtonModel.SetActive(!_ButtonIsOn);
    }

}
