using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[SelectionBase]
[RequireComponent(typeof(AttachController))]
public class GrabbableBlock : Grabbable
{
    private AttachController _cachedAttachController;
    private Endpoints _cachedEndpoints;
    private Transmitter[] _transmitterConnectors;
    private Receiver[] _receiversConnectors;
    private bool _grabbed = false;

    private event Action _blockGrabbedEvent;
    private event Action _blockReleasedEvent;

    void Start() {
        _cachedAttachController = gameObject.GetComponent<AttachController>();
        _cachedEndpoints = gameObject.GetComponentInChildren<Endpoints>();
        _transmitterConnectors = GetComponentsInChildren<Transmitter>();
        _receiversConnectors = GetComponentsInChildren<Receiver>();
    }

    private void FixedUpdate() {
        if (_grabbed == true){
            _cachedAttachController.TryAttachGhost();
        }
    }

    public override GameObject Grab() {
        _grabbed = true;
        DestroyAllCables();
        NotifyGrabbed();
        _cachedAttachController.Detach();
        _cachedEndpoints.ActivateEndpoints();
        return this.transform.root.gameObject;
    }

    public override void Release() {
        _grabbed = false;
        _cachedAttachController.TryAttachItSelf();
        _cachedEndpoints.DeactivateEndpoints();
        NotifyReleased();
    }

    public override void ForceRelease() {
        _grabbed = false;
        NotifyReleased();
        _cachedAttachController.ClearGhost();
        _cachedEndpoints.DeactivateEndpoints();
    }

    public void SubscribeGrabbed( Action OnGrabbed, Action OnReleased){
        _blockGrabbedEvent += OnGrabbed;
        _blockReleasedEvent += OnReleased;
    }

    public void UnsubscribeGrabbed( Action OnGrabbed, Action OnReleased){
        _blockGrabbedEvent -= OnGrabbed;
        _blockReleasedEvent -= OnReleased;
    }

    private void DestroyAllCables(){
        List<GameObject> cablesToDestroy = new List<GameObject>();
        foreach(Transmitter transmitter in _transmitterConnectors){
            foreach(GameObject cable in transmitter.cables){
                cablesToDestroy.Add(cable);
            }
        }
        foreach(Receiver receiver in _receiversConnectors){
            foreach(GameObject cable in receiver.cables){
                cablesToDestroy.Add(cable);
            }
        }
        foreach(GameObject cable in cablesToDestroy){
            Destroy(cable);
        }
    }

    private void  DestroyCables( List<GameObject> cables){
        foreach(GameObject cable in cables){
            Destroy(cable);
        }
    }


    private void NotifyGrabbed(){
        _blockGrabbedEvent?.Invoke();
    }

    private void NotifyReleased(){
        _blockReleasedEvent?.Invoke();
    }
}
