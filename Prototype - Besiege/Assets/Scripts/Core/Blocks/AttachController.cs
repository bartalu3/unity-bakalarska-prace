using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttachController : MonoBehaviour
{
    [SerializeField]
    public GameObject GhostBlockPrefab;
    private GameObject _ghostBlock = null;
    private GhostBlockController _ghostController;
    private Endpoints _endpoints;
    private Vector3 _currentPosition;
    private Quaternion _currentRotation;
    private AttachmentData _currentAttachmentData;

    private void Start(){
        _endpoints = gameObject.GetComponentInChildren<Endpoints>();
    }

    public void TryAttachGhost(){
        _currentAttachmentData = _endpoints.GetClosestToEndpoints();
        if(_currentAttachmentData.closestEndpointObject != null){
            AttachGhost();
        }
        else{
            Destroy(_ghostBlock);
        }
    }

    public void TryAttachItSelf(){
        if(_ghostBlock != null){
            if(_ghostController.IsValidPlacement()){
                transform.position = _ghostBlock.transform.position;
                transform.rotation = _ghostBlock.transform.rotation;
                _endpoints.ConnectToNeighbours();
            }
            Destroy(_ghostBlock);
        }
    }

    public void Detach(){
        _endpoints.DetachAll();
    }

    public void ClearGhost(){
        if(_ghostBlock != null){
            Destroy(_ghostBlock);
        }
    }

    private void AttachGhost(){
        if(_ghostBlock == null){
            _ghostBlock = Instantiate (GhostBlockPrefab, new Vector3(0,0,0), Quaternion.identity);
            _ghostController = _ghostBlock.GetComponent<GhostBlockController>();
            Debug.Log(_ghostController);
            _ghostController.IgnoreCollisionPlacementWith(transform.root.gameObject);
        }
        AttachToEndpoint(_ghostBlock);
    }

    private void AttachToEndpoint(GameObject objectToAttach){
        Vector3 targetEndpointNormal = _currentAttachmentData.closestEndpointObject.transform.rotation * new Vector3(0,0,1);
        Vector3 targetEndpointUp = _currentAttachmentData.closestEndpointObject.transform.rotation * new Vector3(0,1,0);
        Quaternion sourceEndpointLocalRotation = _currentAttachmentData.endpoint.endpointObject.transform.localRotation;
        Quaternion newRotation = Quaternion.LookRotation(-targetEndpointNormal, targetEndpointUp) * Quaternion.Inverse(sourceEndpointLocalRotation);;

        Vector3 rotatedOffsetSourceEndpoint = newRotation * (-_currentAttachmentData.endpoint.mountPosition);
        Vector3 targetEndpointPosition = _currentAttachmentData.closestEndpointObject.transform.position;
        Vector3 newPosition = targetEndpointPosition + rotatedOffsetSourceEndpoint;

        objectToAttach.transform.rotation = newRotation;
        objectToAttach.transform.position = newPosition;

        AngleSnapToEndpoint(objectToAttach);
    }

    private void AngleSnapToEndpoint(GameObject objectToSnap){
        _currentPosition = transform.position;
        _currentRotation = transform.rotation;

        Vector3 targetendpointPosition = _currentAttachmentData.closestEndpointObject.transform.position;
        Vector3 targetendpointNormal = _currentAttachmentData.closestEndpointObject.transform.rotation * new Vector3(0,0,1);

        int rotationWithSmallestDistance = 0;
        float smallestDistance = RotationalDistance(objectToSnap);

        for (int i = 1; i < 4; i++){
            objectToSnap.transform.RotateAround(targetendpointPosition, targetendpointNormal, 90f);
            float distanceToMainBody = RotationalDistance(objectToSnap);
            if(distanceToMainBody < smallestDistance){
                smallestDistance = distanceToMainBody;
                rotationWithSmallestDistance = i;
            }
        }

        int numberOfTimesToRotate = (rotationWithSmallestDistance - 3) % 4;
        objectToSnap.transform.RotateAround(targetendpointPosition, targetendpointNormal, 90f *numberOfTimesToRotate);
    }


    private float RotationalDistance(GameObject objectToSnap){
        float angle = Quaternion.Angle(_currentRotation, objectToSnap.transform.rotation);
        float distance = Vector3.Distance(_currentPosition, objectToSnap.transform.position);
        return angle + distance;
    }
}
