using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostBlockController : MonoBehaviour
{
    public Material validPlacementMaterial;
    public Material invalidPlacementMaterial;

    private MeshRenderer[] _meshRenderers;
    private GhostInvalidPlacementArea _placementArea;

    private bool _invalidPlacement = false;


    private void Awake() {

        _meshRenderers = GetComponentsInChildren<MeshRenderer>();
        _placementArea = GetComponent<GhostInvalidPlacementArea>();
        SetMaterials(validPlacementMaterial);
    }

    private void FixedUpdate() {
        if(!_placementArea.IsEmpty() && _invalidPlacement == false){
            SetMaterials(invalidPlacementMaterial);
            _invalidPlacement = true;
        }
        else if(_placementArea.IsEmpty() && _invalidPlacement == true){
            SetMaterials(validPlacementMaterial);
            _invalidPlacement = false;
        }
    }

    public bool IsValidPlacement(){
        return !_invalidPlacement;
    }

    public void IgnoreCollisionPlacementWith(GameObject ignore){
        Debug.Log(_placementArea);
        _placementArea.AddIgnoredObject(ignore);
    }

    private void SetMaterials(Material mat){

        foreach(MeshRenderer renderer in _meshRenderers){
            Material[] materials = renderer.materials;
            for(int i = 0; i < materials.Length; i++){
                materials[i] = mat;
            }
        }
    }
}
