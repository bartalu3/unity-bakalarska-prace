using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThrusterController : MonoBehaviour
{
    public float thrustForce = 200f;
    public bool thrusterEnabled;
    private ParticleSystem[] particleSystems;
    Rigidbody m_Rigidbody;

    void Start()
    {
        m_Rigidbody = GetComponent<Rigidbody>();
        particleSystems =GetComponentsInChildren<ParticleSystem>();
        TurnOff();
    }

    private void OnEnable(){
        BuildModeChange.buildModeEventOn += TurnOff;
        BuildModeChange.buildModeEventOff += TurnOn;
    }

    private void OnDisable(){
        BuildModeChange.buildModeEventOn -= TurnOff;
        BuildModeChange.buildModeEventOff -= TurnOn;
    }

    void FixedUpdate(){
        if(thrusterEnabled){
            m_Rigidbody.AddForce(transform.forward * thrustForce);
        }
    }

    private void TurnOff(){
        thrusterEnabled = false;
        foreach(ParticleSystem particleSystem in particleSystems){
            particleSystem.Stop();
        }
    }

    private void TurnOn(){
        thrusterEnabled = true;
        foreach(ParticleSystem particleSystem in particleSystems){
            particleSystem.Play();
        }
    }
}
