using UnityEngine;

[RequireComponent(typeof(NibbleInput))]
[RequireComponent(typeof(HingeJoint))]
public class SteerableWheelController : MonoBehaviour
{
    [SerializeField]
    public float maxSteeringAngle;
    public float steeringSpeed;
    private NibbleInput _inputComponent;
    private HingeJoint[] _hingeComponents;
    private JointSpring _spring;

    private void Start() {
        _inputComponent = GetComponent<NibbleInput>();
        _hingeComponents = GetComponents<HingeJoint>();
        _spring = _hingeComponents[0].spring;
    }

    private void FixedUpdate() {
        SetMotorSpeed();
    }

    private void SetMotorSpeed(){
        SNibble nib = _inputComponent.Input;
        float angleScale = (nib.SignedByte) / 7f;
        _spring.targetPosition = -angleScale * maxSteeringAngle;
        _hingeComponents[0].spring = _spring;
        _hingeComponents[1].spring = _spring;
    }
}
