using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostInvalidPlacementArea : MonoBehaviour
{
    public string _objectTag;

    protected HashSet<GameObject> _objectsToIgnore = new HashSet<GameObject>();
    protected HashSet<GameObject> _objectsInTriggerArea = new HashSet<GameObject>();

    public bool IsEmpty(){
        return _objectsInTriggerArea.Count == 0;
    }

    public void AddIgnoredObject(GameObject ignored){
        _objectsToIgnore.Add(ignored);
        foreach(Transform t in ignored.transform){
            _objectsToIgnore.Add(t.gameObject);
        }
    }

    protected virtual void OnTriggerEnter(Collider other) {
        GameObject newEntry = other.transform.gameObject;
        if(_objectsToIgnore.Contains(newEntry)){
            return;
        }
        if(newEntry.tag == _objectTag){
            _objectsInTriggerArea.Add(newEntry);
            return;
        }
        if(newEntry.tag == "Untagged"){
            newEntry = newEntry.transform.root.gameObject;
        }
        if(newEntry.tag == _objectTag){
            _objectsInTriggerArea.Add(newEntry);
        }
    }

    protected virtual void OnTriggerExit(Collider other) {
        GameObject newEntry = other.transform.gameObject;
        if(newEntry.tag == _objectTag){
            _objectsInTriggerArea.Remove(newEntry);
            return;
        }
        if(newEntry.tag == "Untagged"){
            newEntry = newEntry.transform.root.gameObject;
        }
        if(newEntry.tag == _objectTag){
            _objectsInTriggerArea.Remove(newEntry);
        }
    }
}