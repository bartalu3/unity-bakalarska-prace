using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class Endpoint {

    public Vector3 mountPosition;
    public Vector3 normal;
    public GameObject endpointObject;
    public bool connected;
    [System.NonSerialized]
    public List<Endpoint> connectedToEndpoints = new List<Endpoint>();
    public List<FixedJoint> joints = new List<FixedJoint>();

    private static float DISTANCE_NEIGHBOUR_CONSTANT = 0.001f;

    public GameObject GetClosest(){
        return endpointObject.GetComponentInChildren<EndpointObjectsInTriggerArea>().GetClosest();
    }

    public void ConnectToOther(GameObject otherEndpointObject){
        Endpoints otherEndpoints = otherEndpointObject.GetComponentInParent<Endpoints>();
        Endpoint otherEndpoint = otherEndpoints.GetEndpointEntryByGameObject(otherEndpointObject);

        FixedJoint joint = endpointObject.transform.root.gameObject.AddComponent<FixedJoint>();
        joint.connectedBody = otherEndpointObject.transform.root.gameObject.GetComponent<Rigidbody>();

        joints.Add(joint);
        otherEndpoint.joints.Add(joint);
        connected = true;
        otherEndpoint.connected = true;
        connectedToEndpoints.Add(otherEndpoint);
        otherEndpoint.connectedToEndpoints.Add(this);
        UnityEngine.Object.Destroy(endpointObject);
        UnityEngine.Object.Destroy(otherEndpoint.endpointObject);

        endpointObject = null;
        otherEndpoint.endpointObject = null;
    }

    public Endpoints ConnectToNeighbour(){
        GameObject neighbour = GetClosest();
        if(neighbour != null){
            Endpoints parentEndpoints = neighbour.GetComponentInParent<Endpoints>();
            float distance = Vector3.Distance(endpointObject.transform.position, neighbour.transform.position);
            if(distance < DISTANCE_NEIGHBOUR_CONSTANT){
                ConnectToOther(neighbour);
                return parentEndpoints;
            }
        }
        return null;
    }

    public void Detach(){
        RemoveThisFromOtherEndpoints();
        connectedToEndpoints.Clear();
        foreach(FixedJoint joint in joints){
            UnityEngine.Object.Destroy(joint);
        }
        joints.Clear();
        connected = false;
    }

    private void RemoveThisFromOtherEndpoints(){
        foreach (Endpoint otherEndpoint in connectedToEndpoints){
            otherEndpoint.connectedToEndpoints.Remove(this);
            foreach (FixedJoint joint in joints){
                otherEndpoint.joints.Remove(joint);
            }
            if(otherEndpoint.joints.Count == 0){
                otherEndpoint.connected = false;
            }
        }
    }

}
