using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttachmentData{
        public Endpoint endpoint;
        public GameObject closestEndpointObject;
}


public class Endpoints : BuildModeObserver
{
    public GameObject endpointPrefab;
    public GameObject endpointActivePrefab;
    [SerializeField]
    public Endpoint[] endpoints;
    protected List<Endpoints> endpointsToRefresh = new List<Endpoints>();

    private bool _isActive = false;
    private bool _buildModeOn = true;

    private void Start() {
        CreateEndpointObjects();
    }

    public AttachmentData GetClosestToEndpoints(){
        GameObject closestObject = null;
        Endpoint closestEndpoint = null;
        float distanceToClosest = Mathf.Infinity;
        foreach(Endpoint endpoint in endpoints){
            if(endpoint.endpointObject == null){
                continue;
            }
            Vector3 endpointPosition = endpoint.endpointObject.transform.position;
            GameObject currentObject = endpoint.GetClosest();
            if(currentObject != null){
                float distance = Vector3.Distance(endpointPosition, currentObject.transform.position);
                if(distance < distanceToClosest){
                    distanceToClosest = distance;
                    closestObject = currentObject;
                    closestEndpoint = endpoint;
                }
            }
        }
        AttachmentData data = new AttachmentData();
        data.endpoint = closestEndpoint;
        data.closestEndpointObject = closestObject;
        return data;
    }

    public Endpoint GetEndpointEntryByGameObject(GameObject endpointObject){
        foreach(Endpoint endpoint in endpoints){
            if(endpoint.endpointObject == endpointObject){
                return endpoint;
            }
        }
        return null;
    }

    public void ActivateEndpoints(){
        _isActive = true;
        DestroyEndpointObjects();
        CreateEndpointObjects();
    }

    public void DeactivateEndpoints(){
        _isActive = false;
        DestroyEndpointObjects();
        CreateEndpointObjects();
    }


    public void ConnectToNeighbours(){
        foreach(Endpoint endpoint in endpoints){
            if(!endpoint.connected){
                Endpoints otherEndpoints = endpoint.ConnectToNeighbour();
                if(otherEndpoints != null){
                    endpointsToRefresh.Add(otherEndpoints);
                    otherEndpoints.endpointsToRefresh.Add(this);
                }
            }
        }
    }

    public void DetachAll(){
        foreach(Endpoint endpoint in endpoints){
            endpoint.Detach();
        }
        foreach(Endpoints otherEndpoints in endpointsToRefresh){
            otherEndpoints.DisconnectedFrom(this);
        }
        endpointsToRefresh.Clear();
    }

    public void DisconnectedFrom(Endpoints other){
        endpointsToRefresh.Remove(other);
        DestroyEndpointObjects();
        CreateEndpointObjects();
    }

    protected override void BuildModeTurnedOn(){
        _buildModeOn = true;
        CreateEndpointObjects();
    }

    protected override void BuildModeTurnedOff(){
        _buildModeOn = false;
        DestroyEndpointObjects();
    }

    private void CreateEndpointObjects(){
        if(_buildModeOn != true) {
            return;
        }

        for (int i = 0; i < endpoints.Length; i++){
            if(endpoints[i].connected || endpoints[i].endpointObject != null){
                continue;
            }
            GameObject newEndpoint;
            if(_isActive){
                newEndpoint = Instantiate(endpointActivePrefab, new Vector3(0, 0, 0), Quaternion.identity);
            }
            else{
                newEndpoint = Instantiate(endpointPrefab, new Vector3(0, 0, 0), Quaternion.identity);
            }
            newEndpoint.transform.SetParent(this.transform);
            Quaternion rotation = Quaternion.FromToRotation(Vector3.forward, endpoints[i].normal);
            newEndpoint.transform.localRotation = rotation;
            newEndpoint.transform.localPosition = endpoints[i].mountPosition;
            endpoints[i].endpointObject = newEndpoint;
        }
    }

    private void DestroyEndpointObjects(){
        foreach(Endpoint endpoint in endpoints){
            if(endpoint.endpointObject == null){
                continue;
            }
            Destroy(endpoint.endpointObject);
            endpoint.endpointObject = null;
        }
    }

    private void OnDrawGizmosSelected() {
        foreach(Endpoint endpoint in endpoints){
            Vector3 endPointPosition = gameObject.transform.position + endpoint.mountPosition;
            Gizmos.color = Color.green;
            Gizmos.DrawWireSphere(endPointPosition, 0.2f);
            Gizmos.color = Color.red;
            Gizmos.DrawLine(endPointPosition, endPointPosition + endpoint.normal * 0.2f);
        }
    }

}
