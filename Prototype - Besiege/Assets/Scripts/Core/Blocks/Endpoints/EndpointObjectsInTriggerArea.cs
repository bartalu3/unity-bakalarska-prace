using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndpointObjectsInTriggerArea : ObjectsInTriggerArea
{
    protected override void OnTriggerEnter(Collider other) {
        GameObject newEntry = other.transform.gameObject;
        if (newEntry.tag == _objectTag){
            _objectsInTriggerArea.Add(other.transform.gameObject);
        }
    }

    protected override void OnTriggerExit(Collider other) {
        GameObject newEntry = other.transform.gameObject;
        if (newEntry.tag == _objectTag){
            _objectsInTriggerArea.Remove(other.transform.gameObject);
        }
    }
}
