using UnityEngine;

public class NibbleInput : MonoBehaviour
{
    [SerializeField]
    public SNibble nibble = SNibble.ZeroValue;

    private Receivers _receivers;

    public SNibble Input{
        get { return nibble; }
    }

    private void Start()
    {
        _receivers = GetComponentInChildren<Receivers>();
        _receivers.Subscribe(UpdateNibble);
    }

    private void UpdateNibble(SNibble nib) {
        nibble = nib;
    }

}
