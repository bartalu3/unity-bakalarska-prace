using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManualOutput : MonoBehaviour
{
    [SerializeField]
    public SNibble _nibble;
    private Transmitters _transmitters;

    void Start() {
        _transmitters = GetComponentInChildren<Transmitters>();
    }

    void FixedUpdate() {
        _transmitters.SendSignal(_nibble);
    }
}
