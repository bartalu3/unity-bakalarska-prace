using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NibbleOutput : MonoBehaviour
{
    public SNibble _nibble;
    private Transmitters _transmitters;

    protected virtual void Start() {
        _transmitters = GetComponentInChildren<Transmitters>();
    }

    protected virtual void FixedUpdate() {
        _transmitters.SendSignal(_nibble);
    }
}
