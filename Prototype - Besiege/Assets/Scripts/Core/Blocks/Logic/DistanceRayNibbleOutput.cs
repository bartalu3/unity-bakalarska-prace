using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class DistanceRayNibbleOutput : NibbleOutput
{
    [SerializeField]
    public float maxDistance;
    public float castPointDistanceFromCenter;

    private LineRenderer lineRenderer;
    private Vector3 _rayHitPosition;
    private Vector3 _castPoint;

    protected override void Start(){
        base.Start();
        lineRenderer = GetComponent<LineRenderer>();
    }

    protected override void FixedUpdate() {
        CastRayForwardAndMapDistance();
        DrawCastRayLine();
        base.FixedUpdate();
    }

    private void CastRayForwardAndMapDistance(){
        _castPoint = transform.position + transform.forward * castPointDistanceFromCenter;
        Ray rayForward = new Ray(_castPoint, transform.forward);
        RaycastHit rayCastHit;
        if(Physics.Raycast(rayForward, out rayCastHit, maxDistance)){
            _rayHitPosition = rayCastHit.point;
            float distanceFromHit = Vector3.Distance(_rayHitPosition, _castPoint);
            MapDistanceToNibble(distanceFromHit);
        }
        else{
            _nibble = SNibble.ZeroValue;
        }
    }

    private void DrawCastRayLine(){
        lineRenderer.SetPosition(0, _castPoint);
        if(_nibble != SNibble.ZeroValue){
            lineRenderer.startColor = Color.blue;
            lineRenderer.endColor = Color.blue;
            lineRenderer.SetPosition(1, _rayHitPosition);
        }
        else{
            lineRenderer.startColor = Color.cyan;
            lineRenderer.endColor = Color.cyan;
            lineRenderer.SetPosition(1, _castPoint + transform.forward * maxDistance);
        }
    }

    private void MapDistanceToNibble(float distance){
        //maps distance to positive nibble (0-7) closer the object is higher the value
        byte mappedDistance = (byte) Math.Ceiling(((maxDistance - distance) * 7f) / maxDistance);
        _nibble.RawValue = mappedDistance;
    }

    private void OnDrawGizmos() {
        _castPoint = transform.position + transform.forward * castPointDistanceFromCenter;
        if(_nibble != SNibble.ZeroValue){
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(_rayHitPosition, 0.2f);
            Gizmos.color = Color.blue;
            Gizmos.DrawLine(_castPoint, _rayHitPosition);
        }
        else{
            Gizmos.color = Color.cyan;
            Gizmos.DrawLine(_castPoint, _castPoint + transform.forward * maxDistance);
        }

    }
}
