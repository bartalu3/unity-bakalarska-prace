using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaterialTintColorRayNibbleOutput : NibbleOutput
{
    public float maxDistance;
    public float castPointDistanceFromCenter;

    private float _colorTint;
    private Color _grayscaleColor = new Color();
    private RaycastHit _raycastHit;
    private LineRenderer lineRenderer;
    private Vector3 _rayHitPosition;
    private Vector3 _castPoint;

    protected override void Start(){
        base.Start();
        lineRenderer = GetComponent<LineRenderer>();
    }

    protected override void FixedUpdate()
    {
        FireRay();
        TintToNibble(_colorTint);
        DrawCastRayLine();
        base.FixedUpdate();
    }

    private void FireRay() {
        _castPoint = transform.position + transform.forward * castPointDistanceFromCenter;
        Ray rayForward = new Ray(_castPoint, transform.forward);

        if (Physics.Raycast(rayForward, out _raycastHit, maxDistance, -5, QueryTriggerInteraction.Ignore)) {
            Renderer renderer = _raycastHit.collider.GetComponent<MeshRenderer>();
            if(renderer == null){
                renderer =_raycastHit.collider.GetComponentInParent<MeshRenderer>();
            }
            if(renderer == null){
                _nibble = SNibble.ZeroValue;
                return;
            }
            _rayHitPosition = _raycastHit.point;
            _colorTint = renderer.material.color.grayscale;
        }
        else{
            _colorTint = 0;
        }
    }

    private void TintToNibble(float tint){
        _nibble.RawValue = (byte)(tint * 7);
    }

    private void DrawCastRayLine(){
        lineRenderer.SetPosition(0, _castPoint);
        if(_nibble != SNibble.ZeroValue){
            lineRenderer.startColor = _grayscaleColor;
            lineRenderer.endColor = _grayscaleColor;
            lineRenderer.SetPosition(1, _rayHitPosition);
        }
        else{
            lineRenderer.startColor = Color.cyan;
            lineRenderer.endColor = Color.cyan;
            lineRenderer.SetPosition(1, _castPoint + transform.forward * maxDistance);
        }
    }

    private void SetColor(float tint){
        _grayscaleColor.r = tint;
        _grayscaleColor.g = tint;
        _grayscaleColor.b = tint;
    }

    private void OnDrawGizmos() {
        _castPoint = transform.position + transform.forward * castPointDistanceFromCenter;
        if(_nibble != SNibble.ZeroValue){
            Gizmos.color = _grayscaleColor;
            Gizmos.DrawSphere(_raycastHit.point, 0.2f);
            Gizmos.color = Color.magenta;
            Gizmos.DrawLine(_castPoint, _rayHitPosition);
        }
        else{
            Gizmos.color = Color.cyan;
            Gizmos.DrawLine(_castPoint, _castPoint + transform.forward * maxDistance);
        }

    }
}
