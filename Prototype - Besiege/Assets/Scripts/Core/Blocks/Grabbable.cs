using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Grabbable : MonoBehaviour
{
    public abstract GameObject Grab();
    public abstract void Release();
    public abstract void ForceRelease();
}
