using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BuildModeObserver : MonoBehaviour
{
    private void OnEnable() {
        BuildModeChange.buildModeEventOn += BuildModeTurnedOn;
        BuildModeChange.buildModeEventOff += BuildModeTurnedOff;
    }

    private void OnDisable() {
        BuildModeChange.buildModeEventOn -= BuildModeTurnedOn;
        BuildModeChange.buildModeEventOff -= BuildModeTurnedOff;
    }

    protected abstract void BuildModeTurnedOn();
    protected abstract void BuildModeTurnedOff();
}
