using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildModeBlockObserver : BuildModeObserver
{
    private Vector3 _BuildModePosition;
    private Quaternion _BuildModeRotation;
    Rigidbody RigidbodyComponent;

    private void Start() {
        _BuildModePosition = transform.position;
        _BuildModeRotation = transform.rotation;
        RigidbodyComponent = GetComponent<Rigidbody>();
        RigidbodyComponent.constraints = RigidbodyConstraints.FreezeAll;
    }

    protected override void BuildModeTurnedOn(){
        transform.position = _BuildModePosition;
        transform.rotation = _BuildModeRotation;
        RigidbodyComponent.constraints = RigidbodyConstraints.FreezeAll;
    }

    protected override void BuildModeTurnedOff(){
        _BuildModePosition = transform.position;
        _BuildModeRotation = transform.rotation;
        RigidbodyComponent.constraints = RigidbodyConstraints.None;
    }
}
