using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class BuildModeChange : MonoBehaviour
{
    public static event Action buildModeEventOn;
    public static event Action buildModeEventOff;

    public static void TurnBuildModeOn(){
        buildModeEventOn?.Invoke();
    }

    public static void TurnBuildModeOff(){
        buildModeEventOff?.Invoke();
    }
}
