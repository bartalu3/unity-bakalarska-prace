using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectsInTriggerArea : MonoBehaviour
{
    public string _objectTag;
    public HashSet<string> _objectIgnoteTags;

    protected HashSet<GameObject> _objectsToIgnore = new HashSet<GameObject>();
    protected HashSet<GameObject> _objectsInTriggerArea = new HashSet<GameObject>();


    public GameObject GetClosest(){
        GameObject closestObject = null;
        float distanceToClosest = 0;
        foreach(GameObject obj in _objectsInTriggerArea){
            if(obj == null){
                continue;
            }
            if (closestObject == null){
                closestObject = obj;
                distanceToClosest = Vector3.Distance(transform.position, obj.transform.position);
                continue;
            }
            float distance = Vector3.Distance(transform.position, obj.transform.position);
            if(distance < distanceToClosest){
                closestObject = obj;
                distanceToClosest = distance;
            }
        }
        return closestObject;
    }

    public bool IsEmpty(){
        return _objectsInTriggerArea.Count == 0;
    }

    public void AddIgnoredObject(GameObject ignored){
        _objectsToIgnore.Add(ignored);
        foreach(Transform t in ignored.transform){
            _objectsToIgnore.Add(t.gameObject);
        }
    }

    protected virtual void OnTriggerEnter(Collider other) {
        GameObject newEntry = other.transform.gameObject;
        if(_objectsToIgnore.Contains(newEntry)){
            return;
        }
        if(newEntry.tag == _objectTag){
            _objectsInTriggerArea.Add(newEntry);
            return;
        }
        newEntry = other.transform.root.gameObject;
        if (newEntry.tag == _objectTag){
            _objectsInTriggerArea.Add(newEntry);
        }
    }

    protected virtual void OnTriggerExit(Collider other) {
        GameObject newEntry = other.transform.gameObject;
        if(newEntry.tag == _objectTag){
            _objectsInTriggerArea.Remove(newEntry);
            return;
        }
        newEntry = other.transform.root.gameObject;
        if (newEntry.tag == _objectTag){
            _objectsInTriggerArea.Remove(newEntry);
        }
    }
}