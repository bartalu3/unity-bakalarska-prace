using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CurvePointData{
        public Vector3 position;
        public Vector3 tangent;
        public Vector3 normal;
    }

public abstract class Curve
{
    protected float _length;
    protected bool _lengthWasCalculated = false;

    public abstract CurvePointData Interpolate(float t);

    protected abstract void ApproximateLength();

    public float Length{
        get {
            if(_lengthWasCalculated == false){
                ApproximateLength();
                _lengthWasCalculated = true;
            }
            return _length;
            }
    }
}
