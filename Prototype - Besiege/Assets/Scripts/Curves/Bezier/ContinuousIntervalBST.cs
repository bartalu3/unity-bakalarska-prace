using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Search tree for finding interval index
// Interval starts at 0 and are continuously expanded
// Intervals <0; 2>, (2; 3>, (3, 6> will be interpreted as array [0, 2, 3, 6]
// Used for finding on which bezier curve is point located determined by length found

public class ContinuousIntervalBST
{
    List<float> intervals = new List<float>();

    public ContinuousIntervalBST(){
        intervals.Add(0);
    }

    public void AddInterval(float interval){
        intervals.Add(interval);
    }

    public int IndexAt(float num){
        float max = intervals[intervals.Count - 1];
        if (num > max || num < 0){
            throw new ArgumentOutOfRangeException(nameof(num), num, "Out of interval range should be smaller than 0 and greater than " + max.ToString("0.0000"));
        }
        return BinarySearchIntervalIndex(num);
    }

    public Tuple<float, float> GetIntervalAtIndex(int index){
        if (index > intervals.Count - 2 || index < 0){
            throw new IndexOutOfRangeException();
        }
        return Tuple.Create(intervals[index],intervals[index + 1]);
    }

    private int BinarySearchIntervalIndex(float num){
        int low = 0;
        int high = intervals.Count;

        while(low < high){

            int mid = Mathf.FloorToInt((low + high) / 2);
            if(intervals[mid] < num && mid != low){
                low = mid;
            }
            else if(intervals[mid] >= num && mid != high){
                high = mid;
            }
            else{
                low++;
                high = low;
            }
        }
        if(low == 0){
            return 0;
        }
        return low - 1;
    }
}
