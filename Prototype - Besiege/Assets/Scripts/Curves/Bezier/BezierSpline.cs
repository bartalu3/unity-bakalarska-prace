using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BezierSpline : Curve
{

    private List<CubicBezierCurve> _bezierCurves = new List<CubicBezierCurve>();
    private ContinuousIntervalBST _intervalTree;

    public BezierSpline(IList<Vector3> points, Vector3 startControlVector, Vector3 endControlVector){
        if (points.Count <= 1){
            return;
        }
        GenerateCurves(points, startControlVector, endControlVector);
    }

    public override CurvePointData Interpolate(float t){
        CheckInterpolationParameter(ref t);
        float distance = t * Length;
        int bezierToInterpolate = _intervalTree.IndexAt(distance);
        CubicBezierCurve bezierCurve = _bezierCurves[bezierToInterpolate];
        float nomalizedT = LengthToNormalizedBezierCurveParameter(distance, bezierToInterpolate, bezierCurve);

        return bezierCurve.Interpolate(nomalizedT);
    }

    protected override void ApproximateLength(){
        float lengthApproximated = 0;
        _intervalTree = new ContinuousIntervalBST();
        foreach (CubicBezierCurve curve in _bezierCurves){
            lengthApproximated += curve.Length;
            _intervalTree.AddInterval(lengthApproximated);
        }
        _length = lengthApproximated;
    }

    private void CheckInterpolationParameter(ref float t){
        if (t > 1 || t < 0){
            Debug.LogWarning("Interpolation parameter t out of range should not be less than 0 or more than 1. Defaulting to 0.5f");
            t = 0.5f;
        }
    }

    private void GenerateCurves(IList<Vector3> points, Vector3 startControlVector, Vector3 endControlVector){
        Vector3 firstPoint = points[0];
        Vector3 firstControlVector = startControlVector * 1.5f;
        for(int i = 1; i < (points.Count - 1); i++){
            Vector3 secondPoint = points[i];
            Vector3 newControlVector = CalculateControlVector(firstPoint, secondPoint, points[i + 1]);
            _bezierCurves.Add(CreateBezierCurve(firstPoint, secondPoint, firstControlVector, newControlVector));
            firstControlVector = -newControlVector;
            firstPoint = secondPoint;
        }
        _bezierCurves.Add(CreateBezierCurve(firstPoint, points[points.Count - 1], firstControlVector, endControlVector * 1.5f));
    }

    private float LengthToNormalizedBezierCurveParameter(float length, int bezierIndex,CubicBezierCurve bezierCurve){
        Tuple<float, float> interval  = _intervalTree.GetIntervalAtIndex(bezierIndex);
        float bezierCurveLength = bezierCurve.Length;
        float t = (length - interval.Item1) / bezierCurveLength;
        return t;
    }

    private CubicBezierCurve CreateBezierCurve(Vector3 startPoint, Vector3 endPoint, Vector3 startControlVector, Vector3 endControlVector){
        float magnitude = Vector3.Distance(startPoint, endPoint) / 2;
        Vector3 startControlPoint =  startPoint + startControlVector * magnitude;
        Vector3 endControlPoint =  endPoint + endControlVector * magnitude;
        return new CubicBezierCurve(startPoint, endPoint, startControlPoint, endControlPoint);
    }

    private Vector3 CalculateControlVector(Vector3 startPoint, Vector3 midPoint,Vector3 endPoint){
        //Vectors from midPoint defining plane anchored in origin
        Vector3 vectorToStart = startPoint - midPoint;
        Vector3 vectorToEnd = endPoint - midPoint;

        Vector3 normal = Vector3.Cross(vectorToStart, vectorToEnd);
        Vector3 bisector = Vector3.Slerp(vectorToStart, vectorToEnd, 0.5f);

        Vector3 controlVector = Quaternion.AngleAxis(90, normal) * bisector;
        float distanceToStart = Vector3.Distance(controlVector + midPoint, startPoint);
        float distanceToStartInverted = Vector3.Distance(-controlVector + midPoint, startPoint);

        if(distanceToStart > distanceToStartInverted){
            controlVector = -controlVector;
        }
        return controlVector.normalized;
    }

}

