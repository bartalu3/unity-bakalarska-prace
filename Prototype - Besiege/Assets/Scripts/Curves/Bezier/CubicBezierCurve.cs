using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class CubicBezierCurve : Curve
{
    private Vector3 _startPoint;
    private Vector3 _endPoint;
    private Vector3 _startControlPoint;
    private Vector3 _endControlPoint;
    private static int LENGTH_APPROXIMATION_SEGMENTS = 20;
    private static float CLOSE_TANGENT_DISTANCE = 0.005f;

    public CubicBezierCurve(Vector3 startPoint, Vector3 endPoint, Vector3 startControlPoint, Vector3 endControlPoint){
        _startPoint = startPoint;
        _endPoint = endPoint;
        _startControlPoint = startControlPoint;
        _endControlPoint = endControlPoint;
        ApproximateLength();
    }

    protected override void ApproximateLength(){
        float lengthApproximated = 0;
        float start = 0;
        for (int i = 1; i < LENGTH_APPROXIMATION_SEGMENTS + 1; i++){
            float end = i / (float) LENGTH_APPROXIMATION_SEGMENTS;
            lengthApproximated += Vector3.Distance(Position(start),Position(end));
            start = end;
        }
        _length = lengthApproximated;
    }

    public override CurvePointData Interpolate(float t){
        CurvePointData data = new CurvePointData();
        data.position = Position(t);
        data.tangent = Tangent(t);
        data.normal = Vector3.Cross(data.tangent, CloseTangent(t));
        return data;
    }

    private Vector3 Position(float t0){
        float t1 = 1 - t0;
        return t1 * t1 * t1 * _startPoint +
                3 * t1 * t1 * t0 * _startControlPoint +
                3 * t0 * t0 * t1 *_endControlPoint +
               t0 * t0 * t0 * _endPoint;
    }

    private Vector3 Tangent(float t0){
        float t1 = 1 - t0;
        return 3 * t1 * t1 * (_startControlPoint - _startPoint) +
               6 * t1 * t0 * (_endControlPoint - _startControlPoint) +
               3 * t0 * t0 * (_endPoint - _endControlPoint);

    }

    private Vector3 CloseTangent(float t){
        if(t + CLOSE_TANGENT_DISTANCE > 1){
            return -Tangent(t - CLOSE_TANGENT_DISTANCE);
        }
        return Tangent(t + CLOSE_TANGENT_DISTANCE);
    }

}
