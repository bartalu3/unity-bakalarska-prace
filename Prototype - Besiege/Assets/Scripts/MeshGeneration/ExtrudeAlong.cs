using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ExtrudeAlong
{
    protected Vector3[] _vertices = {};
    protected int[] _triangles = {};
    protected Mesh _mesh = new Mesh();

    public virtual Mesh GenerateMesh(MeshShape shape, Curve curve, float resolution = 0.15f){
        int segments = (int)(curve.Length/resolution);
        InitArrays(segments, shape.Count);
        GenerateVertices(shape.DeepCopy(), curve, segments);
        GenerateTriangles(shape.Count);
        _mesh.Clear();
        _mesh.vertices = _vertices;
        _mesh.triangles = _triangles;
        _mesh.RecalculateNormals();
        return _mesh;
    }

    protected void InitArrays(int segments, int verticesInShape){
        _vertices = new Vector3[segments * verticesInShape];
        _triangles = new int [(segments) * verticesInShape * 6];
    }

    protected void GenerateVertices(MeshShape shape, Curve curve, int segments){
        int indexPosition = 0;
        CurvePointData previousData = null;
        for (int i = 0 ; i < segments; i++){
            CurvePointData data = curve.Interpolate(i/(float)(segments-1));
            if(i > 0){
                MinimizeNormalRotation(previousData, data);
            }
            Quaternion rotation = Quaternion.LookRotation(data.normal, data.tangent);
            shape.Position = data.position;
            shape.Rotation = rotation;
            List<Vector3> shapeVertices = shape.GetPoints();
            shapeVertices.ForEach(vertex => _vertices[indexPosition++] = vertex);
            previousData = data;
        }
    }

    protected void GenerateTriangles(int verticesInShape){
        int indexPosition = 0;
        for (int shapeIndex = 1; shapeIndex < _vertices.Length / verticesInShape; shapeIndex++){
            for(int vertex = 0; vertex < verticesInShape; vertex++){
                int currentVertex = vertex + shapeIndex * verticesInShape;
                int neighbourVertex = (vertex + 1) % verticesInShape + shapeIndex * verticesInShape;
                int vertexUnderCurrent = currentVertex - verticesInShape;
                int vertexUnderNeighbour = neighbourVertex - verticesInShape;

                _triangles[indexPosition++] = currentVertex;
                _triangles[indexPosition++] = vertexUnderCurrent;
                _triangles[indexPosition++] = vertexUnderNeighbour;
                _triangles[indexPosition++] = currentVertex;
                _triangles[indexPosition++] = vertexUnderNeighbour;
                _triangles[indexPosition++] = neighbourVertex;
            }
        }
    }

    protected void MinimizeNormalRotation(CurvePointData sourceData, CurvePointData dataToNormalize){
        dataToNormalize.normal = Vector3.ProjectOnPlane(sourceData.normal, dataToNormalize.tangent).normalized;
    }
}
