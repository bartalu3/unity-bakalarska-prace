using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshCircle : MeshShape
{
    public MeshCircle(Vector3 position, Quaternion rotation, float radius, int numberOfSides){
        if(numberOfSides < 3){
            Debug.LogWarning("numberOfSides for a MeshCircle cant be less than 3 defaulting to 3");
            numberOfSides = 3;
        }
        GenerateCircle(numberOfSides, radius);
        _position = position;
        _rotation = rotation;
    }

    private void GenerateCircle(int numberOfSides, float radius){
        Vector3 pointOnCircle = new Vector3 (0, 0, radius);
        for(int i = 0; i < numberOfSides; i++){
            _shapePoints.Add(pointOnCircle);
            pointOnCircle = Quaternion.Euler(0, 360f / numberOfSides, 0) * pointOnCircle;
        }
    }

}
