using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshShape
{
    protected List<Vector3> _shapePoints = new List<Vector3>();
    protected Vector3 _position;
    protected Quaternion _rotation;

    public List<Vector3> GetPoints(){
        List<Vector3> points = new List<Vector3>();
        foreach (Vector3 point in _shapePoints){
            Vector3 pointRotated = _rotation * point;
            pointRotated = _position + pointRotated;
            points.Add(pointRotated);
        }
        return points;
    }

    public MeshShape DeepCopy(){
        MeshShape newShape = new MeshShape();
        newShape._shapePoints = _shapePoints;
        newShape._position = _position;
        newShape._rotation = _rotation;
        return newShape;
    }

    public Vector3 Position{
        set {_position = value;}
    }

    public Quaternion Rotation{
        set {_rotation = value;}
    }

    public int Count{
        get {return _shapePoints.Count;}
    }

}
