using UnityEngine;
using System.Threading;

public class ExtrudeAlongAsync : ExtrudeAlong
{

    protected Thread _workerThread;

    private volatile bool _work = true;
    private volatile bool _workDone = false;
    private volatile bool _workReady = false;
    private MeshShape _shape;
    private Curve _curve;
    private float _resolution;

    ~ExtrudeAlongAsync(){
        _work = false;
    }

    public override Mesh GenerateMesh(MeshShape shape, Curve curve, float resolution = 0.15f){
        if (_workerThread == null){
            _workerThread = new Thread(() => GenerateMeshInThread());
            _workerThread.Start();
        }
        if(_workReady == false){
            _shape = shape;
            _curve = curve;
            _resolution = resolution;
            _workReady = true;
        }
        if(_workDone){
            _mesh.Clear();
            _mesh.vertices = _vertices;
            _mesh.triangles = _triangles;
            _mesh.RecalculateNormals();
            _workDone = false;
        }
        return _mesh;
    }

    protected void GenerateMeshInThread(){
        while(_work){
            if(_workReady == false){
                Thread.Sleep(10);
                continue;
            }
            int segments = (int)(_curve.Length/_resolution);
            InitArrays(segments, _shape.Count);
            GenerateVertices(_shape.DeepCopy(), _curve, segments);
            GenerateTriangles(_shape.Count);
            _workDone = true;
            _workReady = false;
        }
    }

}
