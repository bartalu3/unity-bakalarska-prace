using UnityEditor;
using UnityEngine;
using System;
using System.Collections.Generic;

[CustomPropertyDrawer (typeof(SNibble))]
public class TruthTableDrawer: PropertyDrawer {

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
        string truthInputLabel;
        if(property.propertyPath.Split('[', ']').Length > 1){
            int listIndex = int.Parse(property.propertyPath.Split('[', ']')[1]);
            truthInputLabel = Convert.ToString(listIndex, 2).PadLeft(4, '0');
        }
        else{
            truthInputLabel = "Signed nibble";
        }

        SerializedProperty truthTableProperty  = property.FindPropertyRelative("_value");
        string stringInput = Convert.ToString(truthTableProperty.intValue, 2).PadLeft(4, '0');
        stringInput = EditorGUI.TextField(position, truthInputLabel, stringInput);
        string validatedStringInput = ValidateInput(stringInput);
        truthTableProperty.intValue = ConvertStringToByte(validatedStringInput);
    }

    private string ValidateInput(string input){
        input = input.PadLeft(4, '0');
        string validated = "";
        for(int i = 0; i < 4; i++){
            if(input[i] == '0' || input[i] == '1'){
                validated += input[i];
            }
            else{
                validated += "0";
            }
        }
        return validated;
    }

    private byte ConvertStringToByte(String input){
        if(input.Length > 8){
            throw new ArgumentOutOfRangeException("cannot convert input string with more than 8 chars");
        }

        byte convertedValue = 0;
        byte powPosition = 0;
        for(int i = input.Length - 1; i >= 0; i--){
            byte intValue = CharBitToByte(input[i]);
            convertedValue += (byte)(Math.Pow(2,powPosition) * intValue);
            powPosition++;
        }
        return convertedValue;
    }

    private byte CharBitToByte(Char bit){
        if(bit == '0'){
            return 0;
        }
        else if (bit == '1'){
            return 1;
        }
        throw new ArgumentException("Char must be '0' or '1'");
    }
}