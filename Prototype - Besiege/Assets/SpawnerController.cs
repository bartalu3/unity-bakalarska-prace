using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[SelectionBase]
public class SpawnerController : MonoBehaviour
{
    public float spawnDistance;
    public float snapBackDistance;
    public float rotationalSpeed;
    public float bounceAmplitude;
    public float bounceSpeed;
    public GameObject blockPrefab;
    private GameObject _snappedObject = null;

    private GrabbableBlock _grabableComponent;
    private bool _grabbed = false;



    private void Start(){
        SpawnNewBlock();
    }

    private void FixedUpdate(){
        if(_snappedObject == null){
            SpawnNewBlock();
        }
        else{
            if(Vector3.Distance(_snappedObject.transform.position, transform.position) > snapBackDistance * 2){
                OnReleased();
            }
            BounceRotate();
        }
    }

    private void SpawnNewBlock(){
        Quaternion diagonalRotation = Quaternion.identity;
        diagonalRotation.SetFromToRotation(Vector3.forward, Vector3.one);
        _snappedObject = Instantiate(blockPrefab, transform.position, diagonalRotation);

        _grabableComponent = _snappedObject.GetComponent<GrabbableBlock>();
        _grabableComponent.SubscribeGrabbed(OnGrabbed, OnReleased);
    }

    private void BounceRotate(){
        if(_grabbed == false){
            _snappedObject.transform.RotateAround(_snappedObject.transform.position, Vector3.up, rotationalSpeed * Time.deltaTime);
            Vector3 pos = transform.position;
            pos.y = pos.y + bounceAmplitude * Mathf.Sin(Time.time * bounceSpeed) + spawnDistance;
            _snappedObject.transform.position = pos;
        }
    }

    private void OnGrabbed(){
        _grabbed = true;
    }

    private void OnReleased(){
        _grabbed = false;
        if(Vector3.Distance(_snappedObject.transform.position, transform.position) > snapBackDistance){
            _snappedObject = null;
        }
        _grabableComponent.UnsubscribeGrabbed(OnGrabbed, OnReleased);
    }

    private void OnDrawGizmosSelected() {
        Vector3 pos = new Vector3(gameObject.transform.position.x, gameObject.transform.position.y + spawnDistance, gameObject.transform.position.z);

        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(pos ,0.3f);
        Gizmos.color = Color.grey;
        Gizmos.DrawWireCube(pos, new Vector3(0.6f,2f * bounceAmplitude ,0.6f));
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(pos ,snapBackDistance);
        Gizmos.color = Color.cyan;
        Gizmos.DrawWireSphere(pos ,snapBackDistance * 2);

    }
}
