using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TESTfakehandGrab : MonoBehaviour
{
    public GameObject grabbedBlock;
    [SerializeField]
    bool fakeClick = false;
    private Endpoints kek;
    void Start()
    {
        grabbedBlock.transform.position = this.transform.position;
        FixedJoint joint = gameObject.AddComponent<FixedJoint>();
        joint.connectedBody = grabbedBlock.GetComponent<Rigidbody>();
        grabbedBlock.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
        kek = grabbedBlock.GetComponentInChildren<Endpoints>();
        // Debug.Log(kek);
    }

    // Update is called once per frame
    void Update()
    {
        if(fakeClick == true){
            fakeClick = false;
            // Debug.Log(kek);
            // kek.TryAttach();
        }
    }
}
